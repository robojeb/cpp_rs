use std::path::PathBuf;

use cpp_rs::{
    preprocessor::{Item, Sources},
    util::CStd,
    Preprocessor,
};

fn main() {
    let file = PathBuf::from(std::env::args().nth(1).unwrap());

    let sources = Sources::new(file).unwrap();

    let mut pp = Preprocessor::new_c(&sources, CStd::C99, false, false);

    pp.set_produce_pragma_items(true);

    loop {
        let item = pp.get_next_item();
        println!("{:?}", item);

        match item {
            Item::Done => break,
            _ => {}
        }
    }
}

//! This crate provides a Rust implementation of the C Preprocessor.
//! Functionality is split between two major components, the [Lexer] and
//! the [Preprocessor]. Both of these structs are designed to operate as state
//! machines, producing one token or error at a time.
//!
//! # Lexer
//!
//! The [Lexer] structure turns a sequence of characters from a source into a
//! stream of C Preprocessor [Tokens](lexer::Token).
//!
//! The [Lexer] does not track any context information about the sequence of
//! Tokens it has generated. To properly lex a C/C++ source file
//! an external Lexer driver (typically [Preprocessor]) must toggle
//! context attributes appropriately. Currently the only configurable context
//! information is wheter it is valid to produce
//! [TokenKind::HeaderName](crate::lexer::TokenKind::HeaderName) tokens.
//!
//! This separation reduces the Lexer complexity somewhat, and allows the
//! Lexer to be used more flexibly in case the source file is not C/C++
//! source.
//!
//! ## Lexer features
//! TODO
//!
//! # Preprocessor
//!
//! The [Preprocessor] structure operates on a set of source files managed by a
//! [Sources](preprocessor::source_handling::Sources) structure.
//! It will apply the [Lexer] to the root file and handle C/C++
//! preprocessing rules, like Macro expansion, Token pasting, Stringification.
//! and file inclusion on the token stream.
//!
//! Unlike the [Lexer] which requires input on the state of the file to properly
//! lex tokens, the [Preprocessor] is designed to primarily operate in a
//! "set-and-forget" mode where the [Preprocessor] has its initial state set
//! and then produces the proper stream of tokens.
//!
//! It is possible to use the [Preprocessor] in "interactive" mode where
//! settings (such as what Macros are currently defined) can be changed on the
//! fly by your compiler driver, though this may encounter some
//! performance/memory overhead for some operations
//! (see [preprocessor::PragmaOnceBehavior] for details).
//!
//! ## File Management
//!
//! # Implementation Defined Choices
//!
//! ISO 9899 (The C Programming language standard) defines several choices that
//! each implementation can make while handling a Preprocessing Translation
//! unit. This section of the documentation lays out all the decisions that
//! `cpp_rs` has made.
//!
//! ## Source Character Set
//!
//! This crate uses the UTF-8 encoding as the "Source Character Set" during
//! preprocessing. If an incoming file is not UTF-8 encoded or is improperly
//! UTF-8 encoded, any character which does not map to a valid UTF-8 encoded
//! code-point will be replaced with the Unicode
//! [Replacement Character](https://unicode-table.com/en/FFFD/) "�".
//! A diagnostic [Warning](preprocessor::output::Severity::Warning) will be
//! generated in this case.
//!
//! ## Non-empty sequences of Whitespace
//!
//! This crate treats any non-empty sequence of Whitespace characters as one
//! space. This is mostly apparent when performing stringification of tokens.
//! This means the token sequence `a b     c` will become the string
//! `"a b c"`.
//!
//!
pub mod lexer;
pub mod preprocessor;
pub mod util;

pub use lexer::Lexer;
pub use preprocessor::Preprocessor;

// Reference:
//     * https://www.lysator.liu.se/c/ANSI-C-grammar-l.html
//     * https://github.com/bagder/fcpp
//     * https://github.com/danmar/simplecpp

use std::path::PathBuf;

use super::Trace;
use crate::lexer::ErrorKind as LexerErrorKind;
#[derive(Debug, Clone)]
pub struct Diagnostic {
    pub kind: ErrorKind,
    pub severity: Severity,
    pub trace: Trace,
}

#[derive(Debug, Clone)]
pub enum ErrorKind {
    LexerError(LexerErrorKind),
    MacroNameMustBeAnIdentifier,
    MacroCannotBeModified,
    /// Emitted repeatedly if a fatal error has been generated already
    FatalErrorAlreadyOccurred,
    UnexpectedArguments {
        found: usize,
        expected: usize,
    },
    Io(std::io::ErrorKind),
    IncludeFileNotFound {
        path: PathBuf,
    },
    CouldNotDetermineTime,
    Custom(String),
}

/// Indicates the severity level of a Diagnostic produced during preprocessing
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Severity {
    /// The Diagnostic can not be recovered from and preprocessing will be terminated
    Fatal,
    /// The Diganostic prevents proper generation of output but preprocessing can
    /// continue speculatively.
    ///
    /// If a Diagnostic of this level is emitted final output should not be
    /// produced and any subsequent Diagnostics may be incorrect but can be
    /// produced to reduce the number of subsequent edit-compile cycles needed.
    Error,
    /// The Diagnostic indicates a point of concern during preprocessing
    Warning,
    /// The Diagnostic has informational content only
    Notice,
}

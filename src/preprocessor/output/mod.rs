mod error;
mod token;

use crate::{preprocessor::source_handling::SourceId, util::Span};
pub use error::{Diagnostic, ErrorKind, Severity};
pub use token::{Token, TokenKind};

/// An Item that can be emitted from the `Preprocessor`
#[derive(Debug)]
pub enum Item<'s> {
    /// A single preprocessed token
    Token(Token<'s>),
    /// An error that occurred while processing the file
    Error(Diagnostic),
    /// There was a pragma that the preprocessor didn't consume
    ///
    /// The preprocessor emits `#pragma` items for convenience of downstream
    /// consumers, rather than having to re-parse the stream later.
    Pragma {
        name: Token<'s>,
        args: Vec<Token<'s>>,
    },
    /// No more tokens will be emitted from this point forward
    Done,
}

#[derive(Debug, Clone)]
pub enum Trace {
    /// A "Root" trace item indicating that this token/error came from a source file
    Source {
        id: SourceId,
        span: Span,
    },
    /// A "Root" trace item indicating that this token/error was programmatically generated
    ///
    /// The optional `meta_data` field can be used to help determine what programatic source
    /// the token/error was generated from.
    ///
    /// Span information should be provided and can be either relative to a single
    /// invokation of the generator, or multiple invokations.
    Programmatic {
        meta_data: Option<String>,
        span: Span,
    },

    /// Indicates that this token (or more likely error) occurred while processing
    /// a directive from the other trace.
    WhileProcessingDirective(Box<Trace>),

    /// Indicates this token/error occurred while expanding the macro invocation
    /// at the specified Trace
    WhileExpandingMacro(Box<Trace>),
    TokenPaste(Box<Trace>, Box<Trace>),

    MacroArgument(Box<Trace>),

    /// No information about where this Token/Error came from
    Unknown,
}

impl Trace {
    pub fn try_span(&self) -> Option<Span> {
        match self {
            &Trace::Source { ref span, .. } => Some(span.clone()),
            &Trace::Programmatic { ref span, .. } => Some(span.clone()),
            &Trace::WhileProcessingDirective(ref next)
            | &Trace::WhileExpandingMacro(ref next)
            | &Trace::MacroArgument(ref next)
            | &Trace::TokenPaste(ref next, _) => next.try_span(),

            &Self::Unknown => None,
        }
    }
}

impl<'s> From<Diagnostic> for Item<'s> {
    fn from(e: Diagnostic) -> Self {
        Item::Error(e)
    }
}

impl<'s> From<Token<'s>> for Item<'s> {
    fn from(t: Token<'s>) -> Self {
        Item::Token(t)
    }
}

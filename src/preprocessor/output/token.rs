use super::Trace;
pub use crate::lexer::TokenKind;
use crate::util::Span;
use std::borrow::Cow;

/// A preprocessor token
///
/// Along with the span and token Kind information of a Lexer Token
#[derive(Debug, Clone)]
pub struct Token<'a> {
    /// Span information for this token
    pub trace: Trace,

    /// The kind of token that was produced
    pub kind: TokenKind,

    /// The processed text of the token
    ///
    /// Processing includes removing any line continuations
    pub text: Cow<'a, str>,
}

impl<'a> Token<'a> {
    /// Get span information for this token if available
    pub fn span(&self) -> Option<Span> {
        self.trace.try_span()
    }
}

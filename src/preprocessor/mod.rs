mod implementation;
mod lexer_stack;
pub mod macros;
pub mod output;
pub mod pragmas;
pub mod source_handling;

pub use implementation::Preprocessor;

pub enum PragmaOnceBehavior {
    /// Lazily evaluate `#pragma once` directives as they appear in the file.
    ///
    /// In this mode, `#pragma once` will only suppress tokens which appear
    /// after the directive
    ///
    /// Any tokens which appear before the directive will be produced each time
    /// the file is encountered, for example consider a file `coffee.h`:
    /// ```c
    /// extern int NUM_COFFEES;
    /// #pragma once
    /// bool am_shaky();
    /// ```
    /// The `extern int NUM_COFFEES;` will appear any time this file is
    /// included, but `bool am_shaky();` will only be declared the first time.
    Lazy,

    /// Greedily check for the existence of a `#pragma once` directive
    ///
    /// The preprocessor will look ahead into included files for a line
    /// containing `#pragma once` ignoring other preprocessing operations
    /// entirely.
    ///
    /// This means that a `#pragma once` directive cannot be suppressed by
    /// other items like `#if`, consider the following file `many.h`:
    ///
    /// ```c
    /// #if __COUNTER__ > 10
    /// #pragma once
    /// #endif
    ///
    /// #define INNER_CONCAT(a, b) a##b
    /// #define CONCAT(a, b) INNER_CONCAT(a, b)
    /// #define FOO(n) int CONCAT(foo, __COUNTER__) = n
    ///
    /// FOO(3);
    ///
    /// #include "many.h"
    /// ```
    ///
    /// In `PragmaOnceBehavior::Quick` mode this file will only ever be
    /// included once. Resulting in only the definition of `foo2`.
    Quick,

    /// Accurately check for the existence of a `#pragma once` directive
    ///
    /// The preprocessor will first do a quick check to see if a `#pragma once`
    /// directive exists in the file at all.
    ///
    /// If a directive occurs somewhere in the file, the preprocessor will
    /// then evaluate tokens and directives using the current state of the
    /// preprocessor (current Macros and Settings).
    ///
    /// It will evaluate tokens until it reaches the location of the `#pragma once`.
    /// If at that point in the file the `#pragma once` line is suppressed (by
    /// `#if` or some other directive), the pre-processor will not emit any
    /// tokens from the file.
    ///
    /// If the `#pragma once` is not suppressed at that point in the file the
    /// preprocessor will discard all the tokens produced and resume emitting
    /// tokens from `Preprocessor::get` as if it had not peeked ahead.
    ///
    /// # WARNING
    /// If your compiler driver performs any custom manipulation of the
    /// Preprocessor state while iterating over tokens, eg defining new Macros,
    /// then it is possible that when the location of the `#pragma once` is
    /// reached it will now be considered active.
    ///
    /// In this case the behavior will act like `PragmaOnceBehavior::Lazy` and
    /// the remaining part of the file will be suppressed.
    Full,

    /// Accurately check for the existence of `#pragma once` but don't throw out
    /// the work evaluating its status.
    ///
    /// This mode operates like `PragmaOnceBehavior::Full` but once it reaches
    /// the `#pragma once` if it finds that it is inactive it will submit all
    /// the tokens it has found to the queue.
    ///
    /// This means that any programatic changes by your driver code to the
    /// Preprocessor state (like adding a Macro) will not be reflected on tokens
    /// which occur before the `#pragma once`
    Batch,
}

//! Types implementing Macro's which are required by the C/C++ standard
//!
//!

use super::{Argument, Macro, MacroContext, MacroOutput};
use crate::preprocessor::output::{Diagnostic, Token, TokenKind, Trace};
use std::{
    borrow::{Borrow, Cow},
    collections::HashMap,
    iter::zip,
};
use time::macros::format_description;

pub const UNKNOWN_DATE_STRING: &str = "??? ?? ????";
pub const UNKNOWN_TIME_STRING: &str = "??:??:??";

/// A structure for storing the definition of a standard #define macro
///
/// This can handle "Object Like", "Function Like", and "Variadic" macros defined
/// by `#define` preprocessor directives or defined on the command line
pub struct StandardMacro<'s> {
    is_variadic: bool,
    arg_names: Vec<String>,
    tokens: Vec<Token<'s>>,
}

impl<'s> StandardMacro<'s> {
    /// Create a new "Object Like" macro which replaces its nome with the
    /// provided tokens
    pub fn new(tokens: impl Into<Vec<Token<'s>>>) -> StandardMacro<'s> {
        StandardMacro {
            is_variadic: false,
            arg_names: Vec::new(),
            tokens: tokens.into(),
        }
    }

    /// Create a new "Function Like" macro which replaces its invocation
    /// with the sequence of tokens substituting the provided arguments
    pub fn new_functional(
        arg_names: impl Into<Vec<String>>,
        tokens: impl Into<Vec<Token<'s>>>,
    ) -> StandardMacro<'s> {
        StandardMacro {
            is_variadic: false,
            arg_names: arg_names.into(),
            tokens: tokens.into(),
        }
    }

    /// Create a new "Function Like" macro which replaces its invocation
    /// with the sequence of tokens substituting the provided arguments
    ///
    /// This will substitute any remaining tokens for the `__VA_ARGS__` symbol.
    pub fn new_variadic(
        arg_names: impl Into<Vec<String>>,
        tokens: impl Into<Vec<Token<'s>>>,
    ) -> StandardMacro<'s> {
        StandardMacro {
            is_variadic: true,
            arg_names: arg_names.into(),
            tokens: tokens.into(),
        }
    }
}

impl<'s> Macro<'s> for StandardMacro<'s> {
    fn required_args(&self) -> Option<usize> {
        if !self.is_variadic && self.arg_names.is_empty() {
            None
        } else {
            Some(self.arg_names.len())
        }
    }

    fn process(
        &self,
        ident: Token<'s>,
        args: &[Argument<'s>],
        _ctx: &MacroContext,
    ) -> MacroOutput<'s> {
        if self.arg_names.is_empty() {
            // Short circuit the object like macro
            self.tokens
                .iter()
                .map(|tok| {
                    let mut tok = tok.clone();
                    // Update the trace for this token
                    tok.trace = Trace::WhileExpandingMacro(Box::new(ident.trace.clone()));
                    Ok(tok)
                })
                .collect()
        } else {
            // PANIC SAFETY: It is guaranteed by the `Macro` trait that we will
            // get at least `Self::required_args` items in this array.
            let (args, var_args) = args.split_at(self.arg_names.len());

            if !var_args.is_empty() && !self.is_variadic {
                return vec![Err(Diagnostic {
                    kind: crate::preprocessor::output::ErrorKind::UnexpectedArguments {
                        found: args.len() + var_args.len(),
                        expected: args.len(),
                    },
                    severity: crate::preprocessor::output::Severity::Error,
                    trace: Trace::WhileExpandingMacro(Box::new(ident.trace)),
                })];
            }

            let arg_map: HashMap<&str, _> =
                zip(self.arg_names.iter().map(|s| s.as_ref()), args).collect();

            self.tokens
                .iter()
                .map(|tok| {
                    let tok = if let Some(_arg) = arg_map.get::<str>(tok.text.borrow()) {
                        // Replace with argument
                        unimplemented!()
                    } else if tok.text == "__VA_ARGS__" {
                        unimplemented!()
                    } else {
                        tok.clone()
                    };

                    Ok(tok)
                })
                .collect()
        }
    }
}

/// A macro implementation for the `__LINE__` macro name
///
/// This macro will replace the identifier it is invoked on with the line number
/// at the start of the token. If the invoked token doesn't have line information
/// (eg It was produced programatically) the value `-1` will be returned instead.
pub struct LineMacro;

impl<'s> Macro<'s> for LineMacro {
    fn required_args(&self) -> Option<usize> {
        None
    }

    fn process(
        &self,
        ident: Token<'s>,
        _args: &[Argument<'s>],
        ctx: &MacroContext,
    ) -> MacroOutput<'s> {
        vec![Ok(Token {
            text: Cow::Owned(format!("{}", ctx.line)),
            kind: TokenKind::PPNumber,
            ..ident
        })]
    }
}

/// A Macro which outputs te name of the current file being preprocessed when
/// invoked.
///
/// The produced token will contain a string with the full path used by
/// the `Preprocessor` to open the file.
///
/// # Lossy Conversion
/// The `Preprocessor` operates using the source character set UTF-8.
/// If the path to the current file cannot be properly represented in UTF-8 a
/// lossy version of the path name will be produced replacing illegal characters
/// with the Unicode replacement character "�".
pub struct FileMacro;

impl<'s> Macro<'s> for FileMacro {
    fn required_args(&self) -> Option<usize> {
        None
    }

    fn process(
        &self,
        ident: Token<'s>,
        _args: &[Argument<'s>],
        ctx: &MacroContext,
    ) -> MacroOutput<'s> {
        vec![Ok(Token {
            text: Cow::Owned(format!("\"{}\"", ctx.file.to_string_lossy())),
            kind: TokenKind::StringLiteral,
            ..ident
        })]
    }
}

/// A Macro which outputs the current date when it is invoked.
///
/// The time will be output using the format string
/// `"[month repr:short] [day padding:zero] [year padding:zero repr:full]"` using the
/// format of the `Time` crate.
///
/// If the time cannot be determined a warning will be produced and the
/// token will contain the string `"??? ?? ????"`.
///
/// # Timezones
/// This macro will use the time as reported in the local timezone of the
/// machine running the `Preprocessor`.
///
/// # Default Macro
/// By default this macro will be registered with the `Preprocessor` under the
/// macro name `__DATE__`.
pub struct DateMacro;

impl<'s> Macro<'s> for DateMacro {
    fn required_args(&self) -> Option<usize> {
        None
    }

    fn process(
        &self,
        ident: Token<'s>,
        _args: &[Argument<'s>],
        _ctx: &MacroContext,
    ) -> MacroOutput<'s> {
        match time::OffsetDateTime::now_local()
            .map_err(|_| crate::preprocessor::output::ErrorKind::CouldNotDetermineTime)
            .and_then(|current_time| {
                current_time
                    .format(format_description!(
                        "[month repr:short] [day padding:zero] [year padding:zero repr:full]"
                    ))
                    .map_err(|_| crate::preprocessor::output::ErrorKind::CouldNotDetermineTime)
            }) {
            Ok(date_string) => {
                vec![Ok(Token {
                    kind: TokenKind::StringLiteral,
                    text: Cow::Owned(date_string),
                    trace: Trace::WhileExpandingMacro(Box::new(ident.trace)),
                })]
            }

            Err(err_kind) => {
                vec![
                    Err(Diagnostic {
                        kind: err_kind,
                        severity: crate::preprocessor::output::Severity::Warning,
                        trace: Trace::WhileExpandingMacro(Box::new(ident.trace.clone())),
                    }),
                    Ok(Token {
                        kind: TokenKind::StringLiteral,
                        text: Cow::Borrowed(UNKNOWN_DATE_STRING),
                        trace: Trace::WhileExpandingMacro(Box::new(ident.trace)),
                    }),
                ]
            }
        }
    }
}

/// A Macro which outputs the current time when it is invoked.
///
/// The time will be output using the format string
/// "[hour repr:24]:[minute padding:zero]:[second padding:zero]" using the
/// format of the `Time` crate.
///
/// If the time cannot be determined a warning will be produced and the
/// token will contain the string `"??:??:??"`.
///
/// # Timezones
/// This macro will use the time as reported in the local timezone of the
/// machine running the `Preprocessor`.
///
/// # Default Macro
/// By default this macro will be registered with the `Preprocessor` under the
/// macro name `__TIME__`.
pub struct TimeMacro;

impl<'s> Macro<'s> for TimeMacro {
    fn required_args(&self) -> Option<usize> {
        None
    }

    fn process(
        &self,
        ident: Token<'s>,
        _args: &[Argument<'s>],
        _ctx: &MacroContext,
    ) -> MacroOutput<'s> {
        match time::OffsetDateTime::now_local()
            .map_err(|_| crate::preprocessor::output::ErrorKind::CouldNotDetermineTime)
            .and_then(|current_time| {
                current_time
                    .format(format_description!(
                        "[hour repr:24]:[minute padding:zero]:[second padding:zero]"
                    ))
                    .map_err(|_| crate::preprocessor::output::ErrorKind::CouldNotDetermineTime)
            }) {
            Ok(date_string) => {
                vec![Ok(Token {
                    kind: TokenKind::StringLiteral,
                    text: Cow::Owned(date_string),
                    trace: Trace::WhileExpandingMacro(Box::new(ident.trace)),
                })]
            }

            Err(err_kind) => {
                vec![
                    Err(Diagnostic {
                        kind: err_kind,
                        severity: crate::preprocessor::output::Severity::Warning,
                        trace: Trace::WhileExpandingMacro(Box::new(ident.trace.clone())),
                    }),
                    Ok(Token {
                        kind: TokenKind::StringLiteral,
                        text: Cow::Borrowed(UNKNOWN_TIME_STRING),
                        trace: Trace::WhileExpandingMacro(Box::new(ident.trace)),
                    }),
                ]
            }
        }
    }
}

use super::{Argument, Macro, MacroContext, MacroOutput};
use crate::preprocessor::output::{Token, TokenKind};
use std::{borrow::Cow, sync::atomic::AtomicU64};
/// A Macro which outputs an incrementing `TokenKind::PPNumber` every time it is called
///
/// # Default Macro
/// By default this is registered with the `Preprocessor` under the macro name
/// `__COUNTER__`, though if you require additional separate incrementing streams
/// you can register a new instance of this macro under another name.
pub struct CounterMacro {
    value: AtomicU64,
}

impl std::default::Default for CounterMacro {
    fn default() -> Self {
        CounterMacro {
            value: AtomicU64::new(0),
        }
    }
}

impl<'s> Macro<'s> for CounterMacro {
    fn required_args(&self) -> Option<usize> {
        None
    }

    fn process(
        &self,
        ident: Token<'s>,
        _args: &[Argument<'s>],
        _ctx: &MacroContext,
    ) -> MacroOutput<'s> {
        // We are okay with wrapping behavior here to prevent an ICE if someone somehow
        // invokes this `u64::MAX` times (which probably should never happen) but
        // because our tokenizer can theoretically accept programmatic input (any CharSource type is fine)
        // someone could do this.
        let count = self
            .value
            .fetch_add(1, std::sync::atomic::Ordering::Relaxed);

        vec![Ok(Token {
            kind: TokenKind::PPNumber,
            text: Cow::Owned(format!("{}", count)),
            ..ident
        })]
    }
}

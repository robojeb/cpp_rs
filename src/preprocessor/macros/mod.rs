//! Traits and Types for implementing Macro expansions in the `Preprocessor`
//!
//!

pub mod common;
pub mod standard;

use super::output::{Diagnostic, Token};
use std::{borrow::Cow, path::PathBuf};

/// Set of names that are not allowed to be defined (or undefined) as a macro when preprocessing C files
pub const DISALLOWED_MACRO_NAMES_C: &[&str] = &["defined"];

/// Set of names that are not allowed to be defined (or undefined) as a macro when preprocessing C++ files
pub const DISALLOWED_MACRO_NAMES_CPP: &[&str] = &[
    "defined", "and", "and_eq", "bitand", "bitor", "compl", "not", "not_eq", "or", "or_eq", "xor",
    "xor_eq",
];

/// The output type for a Macro
pub type MacroOutput<'s> = Vec<Result<Token<'s>, Diagnostic>>;

/// A single argument provided to a Macro
///
/// Arguments can consist of one or more tokens excluding surrounding whitespace
/// and the comma separator.
pub type Argument<'s> = Vec<Token<'s>>;

/// Context information provided to Macro expansion types
pub struct MacroContext {
    pub line: usize,
    pub file: PathBuf,
}

pub struct ActualArgument<'s> {
    tokens: Vec<Token<'s>>,
}

impl<'s> ActualArgument<'s> {
    /// Perform stringification on the tokens provided by the argument
    pub fn stringify(&self) -> Cow<'s, str> {
        unimplemented!()
    }

    pub fn expand(&self) -> ActualArgument<'s> {
        unimplemented!()
    }
}

/// A trait for defining C-preprocessor macros
///
/// This trait can be implemented by other types and added to the Preprocessor
/// to allow general procedural macros within the C-preprocessor
///
/// A macro will be registered with the `Preprocessor` with a name, when an identifier
/// with that name appears in a valid context the `Macro::process` code will be invoked.
pub trait Macro<'s> {
    /// Report how many arguments the macro wants so the preprocessor can
    /// gather those arguments prior to macro invocation
    ///
    /// Reporting `None` will cause the preprocessor to only consume
    /// the identifier token, any `Some(_)` value will cause the preprocessor to look
    /// for a parenthesized list of arguments eg `IDENT(A, B, C)`
    fn required_args(&self) -> Option<usize>;

    /// Process the macro with the provided arguments
    ///
    /// The preprocessor will provide at least `self.required_args()` arguments
    /// but may provide more if available. This function is responsible for
    /// reporting an error if too many arguments are provided
    fn process(
        &self,
        ident: Token<'s>,
        args: &[Argument<'s>],
        ctx: &MacroContext,
    ) -> MacroOutput<'s>;
}

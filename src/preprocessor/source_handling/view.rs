use std::{marker::PhantomData, sync::Arc};

use crate::util::{traits::CharSource, CharInfo, IdxInfo};

/// A Read only view into source code stored in a [Sources] cache
pub struct SourceView<'s> {
    cur_char: usize,
    cur_byte: usize,
    data: Arc<String>,
    _lif: PhantomData<&'s ()>,
}

impl<'s> SourceView<'s> {
    pub(super) fn from_arc(arc: &Arc<String>) -> SourceView<'s> {
        SourceView {
            cur_byte: 0,
            cur_char: 0,
            data: arc.clone(),
            _lif: PhantomData,
        }
    }
}

impl<'s> CharSource<'s> for SourceView<'s> {
    fn next(&mut self) -> Option<CharInfo> {
        if let Some(char) = self.data[self.cur_byte..].chars().next() {
            let width = char.len_utf8();

            let idx = self.cur_char;
            let byte = self.cur_byte;

            self.cur_char += 1;
            self.cur_byte += width;

            Some(CharInfo {
                char,
                idx: IdxInfo {
                    byte,
                    char_index: idx,
                },
            })
        } else {
            None
        }
    }

    fn get_slice(&mut self, start: IdxInfo, end: IdxInfo) -> std::borrow::Cow<'s, str> {
        // SAFETY?: We know that the string will exist as long as the `Sources` struct
        // exists so returning a slice with the lifetime associated with this should
        // be fine. Maybe we should run it by /r/rust
        std::borrow::Cow::Borrowed(unsafe {
            std::mem::transmute(
                self.data
                    .get(start.byte..=end.byte)
                    .expect("ICE: Failed to extract source slice"),
            )
        })
    }

    fn get_byte_len(&self) -> usize {
        self.data.as_bytes().len()
    }

    fn get_char_len(&self) -> usize {
        // BUG: This is wrong, the String type len operation returns byte len
        self.data.len()
    }
}

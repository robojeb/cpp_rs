mod view;

pub use self::view::SourceView;
use super::output::ErrorKind;
use std::{
    collections::HashMap,
    fs::File,
    io::Read,
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
};

/// ID type used to track sources stored in a [SourceCache]
#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SourceId(pub(crate) IdType);

/// Internal type for source identification
type IdType = u16;

/// Caches all the required source files for preprocessing
///
/// Source file text can be recalled by provided the Id which was given when
/// the source was added.
///
/// This structure utilizes internal mutability to allow adding new sources while
/// having outstanding `SourceView` items.
pub struct Sources {
    root: SourceId,
    inner: Mutex<Inner>,
}

impl Sources {
    /// Create a new set of sources with the specified file as the root
    ///
    /// The root file is used by a [Preprocessor](create::Preprocessor) instance
    /// as the starting point for compilation. Multiple Preprocessors can share
    /// one cache by using `Sources::share_new_root()` to create a new
    /// Sources structure with the same cache but a different root file.
    pub fn new<P: AsRef<Path>>(root: P) -> Result<Sources, std::io::Error> {
        Ok(Sources {
            root: SourceId(0),
            inner: {
                let mut ids = HashMap::new();
                ids.insert(SourceId(0), SourceName::File(root.as_ref().into()));

                let mut file = HashMap::new();

                file.insert(SourceId(0), Arc::new(std::fs::read_to_string(root)?));
                Mutex::new(Inner {
                    next: 1,
                    names: ids,
                    file,
                    aux: HashMap::new(),
                })
            },
        })
    }

    pub fn share_new_root(&self) -> Result<Sources, std::io::Error> {
        unimplemented!()
    }

    pub fn get_source_name(&self, id: SourceId) -> SourceName {
        let inner = self.inner.lock().unwrap();

        inner.names.get(&id).unwrap().clone()
    }

    pub fn get_root_id(&self) -> SourceId {
        self.root
    }

    pub fn get_root(&self) -> SourceView {
        self.get_source(self.get_root_id())
    }

    pub fn get_source(&'_ self, id: SourceId) -> SourceView<'_> {
        let inner = self.inner.lock().expect("Couldn't lock sources inner item");

        inner
            .file
            .get(&id)
            .or_else(|| inner.aux.get(&id))
            .map(SourceView::from_arc)
            .expect("ICE: Requested source didn't exist.")
    }

    pub fn add_file_source<P: AsRef<Path>>(
        &self,
        path: P,
    ) -> Result<(SourceId, SourceView), ErrorKind> {
        let mut inner = self.inner.lock().unwrap();

        // Check if this source already exists
        if let Some(existing) = inner.names.iter().find_map(|(id, name)| match name {
            SourceName::File(candidate) if path.as_ref() == candidate => Some(*id),
            _ => None,
        }) {
            std::mem::drop(inner);
            return Ok((existing, self.get_source(existing)));
        }

        // Try to open, read, and validate the file
        let mut file = File::open(&path).map_err(|err| ErrorKind::Io(err.kind()))?;

        let mut buffer = String::new();
        // This reads and validates in one go, in the future we might want
        // to support different encodings so we would read to a binary buffer
        // and perform validation/translation at that time
        file.read_to_string(&mut buffer)
            .map_err(|err| ErrorKind::Io(err.kind()))?;

        // Get a new ID
        let id = inner.next;
        inner.next = inner
            .next
            .checked_add(1)
            .expect("ICE: Could not allocate a new source ID");

        // Insert the name and data
        inner
            .names
            .insert(SourceId(id), SourceName::File(path.as_ref().into()));

        inner.file.insert(SourceId(id), Arc::new(buffer));

        std::mem::drop(inner);

        // Return the new file
        Ok((SourceId(id), self.get_source(SourceId(id))))
    }

    pub fn add_aux_source<N: Into<String>, S: Into<String>>(
        &'_ self,
        name: N,
        source: S,
    ) -> (SourceId, SourceView<'_>) {
        let mut inner = self.inner.lock().unwrap();

        let id = inner.next;
        inner.next = inner
            .next
            .checked_add(1)
            .expect("ICE: Could not allocate a new source ID");

        inner
            .names
            .insert(SourceId(id), SourceName::Aux(name.into()));
        inner.aux.insert(SourceId(id), Arc::new(source.into()));

        std::mem::drop(inner);

        (SourceId(id), self.get_source(SourceId(id)))
    }
}

#[derive(Clone)]
pub enum SourceName {
    File(PathBuf),
    Aux(String),
}

struct Inner {
    next: IdType,
    names: HashMap<SourceId, SourceName>,
    file: HashMap<SourceId, Arc<String>>,
    aux: HashMap<SourceId, Arc<String>>,
}

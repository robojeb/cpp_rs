//! Public interface to the preprocessor
//!

use super::{Preprocessor, State};
use crate::lexer::{Lexer, Token as LexerToken};
use crate::preprocessor::pragmas::PragmaContext;
use crate::preprocessor::PragmaOnceBehavior;
use crate::preprocessor::{
    lexer_stack::LexerStack,
    macros::{self, standard::StandardMacro, Macro},
    output::{Diagnostic, ErrorKind, Item, Severity, Token, TokenKind, Trace},
    pragmas::Pragma,
    source_handling::{SourceName, SourceView, Sources},
};
use crate::util::{CStd, CppStd};
use std::collections::{HashMap, HashSet, VecDeque};

impl<'s> Preprocessor<'s> {
    /// Create a preprocessor instance over a collection of sources
    ///
    /// This instance will have no predefined macros or pragmas but will handle
    /// all other C/C++ directives.
    ///
    /// # Note #
    /// The `sources` struct offers interior mutability and may be modified by
    /// the preprocessor
    pub fn new(sources: &'s Sources) -> Preprocessor<'s> {
        let local_include_paths = match sources.get_source_name(sources.get_root_id()) {
            SourceName::Aux(_) => Vec::new(),
            SourceName::File(p) => vec![p],
        };

        Preprocessor {
            state: State::LineStart,
            sources,
            stack: LexerStack::new(sources.get_root_id(), sources.get_root()),
            staged_emit: VecDeque::with_capacity(10),
            emit_queue: VecDeque::with_capacity(10),

            local_include_paths,
            system_include_paths: Vec::with_capacity(5),

            produce_pragma_items: true,
            restricted_macro_names: HashSet::with_capacity(10),
            use_cpp_named_operators: false,

            macros: HashMap::with_capacity(40),
            pragmas: HashMap::with_capacity(20),
            pragma_supress_tokens: HashMap::with_capacity(20),
        }
    }

    /// Create a new preprocesssor instance configured for C sources
    ///
    /// # Macros:
    ///   * `__LINE__`: Expands to the line where this macro is expanded
    ///   * `__FILE__`: Expands to the path to the current file
    ///   * `__TIME__`: Expands to the current time in the format "HH:MM:SS"
    ///   * `__DATE__`: Expands to the current date in the format "Mth DD YYYY"
    ///   * `__COUNTER__`: Expands to a sequentially increasing integer
    ///   * `__STDC_VERSION__`: Expands to an integer of the form `yyyymmL` where `yyyy` and `mm` are the year and month of the standard's release
    ///   * `__STDC__`: Expands to "1" if the `stdc_strict` argument is `true` otherwise undefined
    ///   * `__STDC_HOSTED__`: Expands to "1" if the `stdc_hosted` argument is `true` otherwise undefined
    ///
    /// ## Restricted macro names
    /// The preprocessor built-in function name "defined" cannot be overwritten or undefined.
    ///
    /// # Pragmas
    /// The following pragma's are included by default
    ///
    ///   * `once`: Prevents a source file from being processed multiple times even if `#include`d in multiple places
    ///   * `push <macroname>`: Push the current value of the named macro onto a stack, leaving the macro undefined
    ///   * `pop <macroname>`: Pop the specified macro off the stack and overwrite its current value
    ///
    pub fn new_c(
        sources: &'s Sources,
        version: CStd,
        stdc_strict: bool,
        stdc_hosted: bool,
    ) -> Preprocessor<'s> {
        let mut out = Self::new(sources);

        // Standard common definitions
        out.add_common_macros();
        out.add_pragmas();

        out.add_define("__STDC_VERSION__", version.get_expand());
        if stdc_strict {
            out.add_define("__STDC__", "1");
        }

        if stdc_hosted {
            out.add_define("__STDC_HOSTED__", "1");
        }

        out.set_restricted_macro_names(macros::DISALLOWED_MACRO_NAMES_C);

        out
    }

    /// Create a new preprocesssor instance configured for C++ sources
    ///
    /// # Included preprocessor macros:
    ///   * `__LINE__`: Expands to the line where this macro is expanded
    ///   * `__FILE__`: Expands to the path to the current file
    ///   * `__TIME__`: Expands to the current time in the format "HH:MM:SS"
    ///   * `__DATE__`: Expands to the current date in the format "Mth DD YYYY"
    ///   * `__COUNTER__`: Expands to a sequentially increasing integer
    ///   * `__cplusplus`: Expands to an integer of the form `yyyymmL` where `yyyy` and `mm` are the year and month of the standard's release
    ///   * `__STDC__`: Expands to "1" if the `stdc_strict` argument is `true` otherwise undefined
    ///   * `__STDC_HOSTED__`: Expands to "1" if the `stdc_hosted` argument is `true` otherwise undefined
    ///
    /// ## Restricted macro names
    /// The preprocessor built-in function name "defined" cannot be overwritten or undefined.
    /// Additionally the C++ named operators: "and", "and_eq", "bitand", "bitor", "compl", "not", "not_eq", "or", "or_eq", "xor", and "xor_eq"
    /// cannot be overwritten or undefined.
    ///
    /// # Pragmas
    /// The following pragma's are included by default
    ///
    ///   * `once`: Prevents a source file from being processed multiple times even if `#include`d in multiple places
    ///   * `push <macroname>`: Push the current value of the named macro onto a stack, leaving the macro undefined
    ///   * `pop <macroname>`: Pop the specified macro off the stack and overwrite its current value
    ///
    pub fn new_cpp(
        sources: &'s Sources,
        version: CppStd,
        stdc_strict: bool,
        stdc_hosted: bool,
    ) -> Preprocessor<'s> {
        let mut out = Self::new(sources);

        out.add_common_macros();
        out.add_pragmas();

        // Define the tokens produced by the `__cplusplus` macro
        out.add_define("__cplusplus", version.get_expand());
        if stdc_strict {
            out.add_define("__STDC__", "1");
        }

        if stdc_hosted {
            out.add_define("__STDC_HOSTED__", "1");
        }

        out.set_restricted_macro_names(macros::DISALLOWED_MACRO_NAMES_CPP);

        out
    }

    /// Set a set of restricted macro names
    ///
    /// The `Self::add_macro`, `Self::remove_macro`, and `Self::add_define` functions will return an error
    /// if any of these names are modified. Any directive which attempts to use these functions
    /// will similarly report an error.
    pub fn set_restricted_macro_names<N: IntoIterator<Item = S>, S: AsRef<str>>(
        &mut self,
        names: N,
    ) {
        self.restricted_macro_names = names.into_iter().map(|x| x.as_ref().into()).collect();
    }

    /// Add a macro definition to the preprocessor returning an existing macro if it exists.
    /// This will ignore restricted macro names.
    pub fn add_macro_unchecked<N: Into<String>, M: Macro<'s> + 's>(
        &mut self,
        name: N,
        macro_def: M,
    ) -> Option<Box<dyn Macro<'s> + 's>> {
        self.macros.insert(name.into(), Box::new(macro_def))
    }

    /// Add a macro definition to the preprocessor.
    ///
    /// This will return the previous macro value if it exists, or an error if the macro name is restricted
    pub fn add_macro<N: Into<String>, M: Macro<'s> + 's>(
        &mut self,
        name: N,
        macro_def: M,
    ) -> Result<Option<Box<dyn Macro<'s> + 's>>, Diagnostic> {
        let name = name.into();
        if self.restricted_macro_names.contains(&name) {
            Err(Diagnostic {
                kind: ErrorKind::MacroCannotBeModified,
                trace: Trace::Unknown,
                severity: Severity::Error,
            })
        } else {
            Ok(self.add_macro_unchecked(name, macro_def))
        }
    }

    /// Remove (or undefine) a macro in the preprocessor context, if a macro exists it will be returned.
    /// This will ignore restricted macro names.
    pub fn remove_macro_unchecked<N: Into<String>>(
        &mut self,
        name: N,
    ) -> Option<Box<dyn Macro<'s> + 's>> {
        self.macros.remove(&name.into())
    }

    /// Remove (or undefine) a macro in the preprocessor.
    ///
    /// This will return the previous macro value if it exists, or an error if the macro name is restricted
    pub fn remove_macro<N: Into<String>>(
        &mut self,
        name: N,
    ) -> Result<Option<Box<dyn Macro<'s> + 's>>, Diagnostic> {
        let name = name.into();
        if self.restricted_macro_names.contains(&name) {
            Err(Diagnostic {
                kind: ErrorKind::MacroCannotBeModified,
                trace: Trace::Unknown,
                severity: Severity::Error,
            })
        } else {
            Ok(self.remove_macro_unchecked(name))
        }
    }
    /// Add a new define macro with `name` and associated `source` used as its source-code
    ///
    /// This a convenience wrapper around:
    /// ```ignore
    /// preprocessor.add_define_from_named_source(macro_name, macro_name, macro_source)
    /// ```
    ///
    /// This macro will accept no arguments. The source code provided will be added to
    /// the `Sources` struct and assigned its own `SourceID`.
    ///
    /// This function is provided as a helper and is equivalent to tokenizing the provided
    /// source and then calling `Self::add_macro(StandardMacro::new(tokens))`
    #[inline]
    pub fn add_define<N: Into<String>, S: Into<String>>(&mut self, macro_name: N, macro_source: S) {
        let macro_name = macro_name.into();
        self.add_define_from_named_source(macro_name.clone(), macro_name, macro_source);
    }

    /// Add a new define macro with `macro_name` and associated `macro_source` used as its source-code provide `source_name` as the name to store in the `Sources` cache
    ///
    /// This macro will accept no arguments. The source code provided will be added to
    /// the `Sources` struct and assigned its own `SourceID`.
    ///
    /// This function is provided as a helper and is equivalent to tokenizing the provided
    /// source and then calling `Self::add_macro(StandardMacro::new(tokens))`
    ///
    /// ```ignore
    /// preprocessor.add_define_from_named_source("Command Line(0)", "CONFIG_ARG", "1")
    /// ```
    ///
    #[inline]
    pub fn add_define_from_named_source<N: Into<String>, S: Into<String>, P: Into<String>>(
        &mut self,
        source_name: P,
        macro_name: N,
        macro_source: S,
    ) {
        let (id, view): (_, SourceView<'s>) = self
            .sources
            .add_aux_source(source_name.into(), macro_source);

        let define_tokenizer: Lexer<'s, SourceView<'s>> = Lexer::new(view);

        let mut tokens = Vec::new();

        for lexer_tok in define_tokenizer {
            match lexer_tok {
                Ok(LexerToken {
                    kind: TokenKind::EOF,
                    ..
                }) => break,
                Ok(tok) => tokens.push(Token {
                    kind: tok.kind,
                    trace: Trace::Source { id, span: tok.span },
                    text: tok.text,
                }),
                Err(e) => panic!("Error while processing define: {:?}", e),
            }
        }

        self.add_macro(macro_name.into(), StandardMacro::new(tokens))
            .expect("Could not modify macro");
    }

    /// Register's a new pragma handler for the given name
    pub fn register_pragma<N: Into<String>, P: Pragma<'s> + 's>(&mut self, name: N, pragma: P) {
        let name = name.into();
        self.pragmas.insert(name.clone(), Box::new(pragma));
        self.pragma_supress_tokens.insert(name, false);
    }

    /// Set whether `#pragma` directives should be emitted as a stream of tokens
    /// or as a single `Item::Pragma`
    pub fn set_produce_pragma_items(&mut self, should_produce: bool) {
        self.produce_pragma_items = should_produce;
    }

    /// Set whether the preprocessor should scan new files for `#pragma once` before beginning tokenization
    ///
    /// See the documentation for [PragmaOnceBehavior] for details on each option.
    pub fn set_pragma_once_behavior(&mut self, _behavior: PragmaOnceBehavior) {
        unimplemented!()
    }

    /// Get the next `Item` from the stream
    ///
    /// If the user has set `Self::produce_pragma_items(true)` then `Items` may
    /// include `Item::Pragma` definitions for the downstream code to consume as
    /// a single item, otherwise uknown `#pragma` directives will be returned
    /// as individual tokens.
    pub fn get_next_item(&mut self) -> Item<'s> {
        loop {
            // Check if a preprocessed token is already available
            if let Some(to_emit) = self.emit_queue.pop_front() {
                match &to_emit {
                    &Item::Token(
                        ref tok @ Token {
                            kind: TokenKind::EOF,
                            ..
                        },
                    ) => {
                        // Invoke EOF for all pragmas
                        for (pragma_name, pragma) in self.pragmas.iter_mut() {
                            let ctx = PragmaContext {
                                restricted_macro_names: &mut self.restricted_macro_names,
                                are_tokens_supressed: false,
                                suppress_token_output: self
                                    .pragma_supress_tokens
                                    .get_mut(pragma_name)
                                    .unwrap(),
                                macros: &mut self.macros,
                            };

                            pragma.on_eof(tok, self.stack.current_file_id(), ctx)
                        }

                        // Check if iteration should finish
                        if self.stack.current_file_id() == (0, self.sources.get_root_id()) {
                            self.state = State::Done;
                        }

                        continue;
                    }
                    // If any pragma (or if) has tokens supressed do not emit
                    // tokens
                    _ if self.pragma_supress_tokens.values().any(|b| *b) => continue,

                    // Check for possible macro expansion of token items
                    Item::Token(ref _tok) => {
                        todo!("Check for macro expansion")
                    }

                    // Emit errors
                    _ => {}
                }
                return to_emit;
            }

            let temp_state = self.state.clone();
            match temp_state {
                State::LineStart => self.handle_line_start(),
                State::NormalLine => self.handle_normal_line(),
                State::EmitPragma => self.handle_emit_pragma(),
                // Once we fall into a Fatal error we will emit it and then be done
                State::Fatal(e) => {
                    self.state = State::Done;
                    return Item::Error(e);
                }
                State::Done => return Item::Done,
                _ => unimplemented!(),
            }
        }
    }
}

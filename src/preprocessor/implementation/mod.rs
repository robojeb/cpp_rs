mod private;
pub mod public;

use super::{
    lexer_stack::LexerStack,
    macros::Macro,
    output::{Diagnostic, Item, Token},
    pragmas::Pragma,
    source_handling::Sources,
};
use std::{
    collections::{HashMap, HashSet, VecDeque},
    path::PathBuf,
};

// References:
// [gcc]: https://gcc.gnu.org/onlinedocs/cpp/index.html#Top

/// The preprocessor state-machine
///
///
pub struct Preprocessor<'s> {
    /// The current state-machine state for the preprocessor
    state: State,

    /// Reference to the sources that have been used for preprocessing
    sources: &'s Sources,

    /// Stack of all the input files and unprocessed tokens
    stack: LexerStack<'s>,

    /// "System" include paths
    system_include_paths: Vec<PathBuf>,

    /// "Local" include paths
    local_include_paths: Vec<PathBuf>,

    /// Tokens that we have consumed but don't know if they need to emit yet
    staged_emit: VecDeque<Item<'s>>,

    /// A queue of Items which are ready to be emitted, this is generally used
    /// when multiple errors need to be generated in a single step and allows
    /// some simplification of the logic.
    emit_queue: VecDeque<Item<'s>>,

    /*
     * Configuration information
     */
    /// Whether unknown `#pragma` directives should be emitted as `Item::Pragma` or
    /// a sequence of `Item::Token`s.
    produce_pragma_items: bool,

    /// What macro names are not allowed to be overwritten or undefined
    restricted_macro_names: HashSet<String>,

    /// Whether macro conditionals should use the C++ named operators
    use_cpp_named_operators: bool,

    /// Currently defined macros
    macros: HashMap<String, Box<dyn Macro<'s> + 's>>,
    /// Currently defined pragmas
    pragmas: HashMap<String, Box<dyn Pragma<'s> + 's>>,
    /// Keep track of whether each pragma wants to supress output
    pragma_supress_tokens: HashMap<String, bool>,
}

/// Private preprocessor state information
#[derive(Clone)]
enum State {
    /// We are at the beginning of a line (modulo comments and whitespace)
    /// Items like preprocessor directives are valid to find here
    LineStart,

    /// We determined this line does not have a directive so just emit the
    /// tokens expanding when needed
    NormalLine,

    /// We found an unknown pragma and have been instructed to emit it as a
    /// stream of tokens rather than as a pragma item.
    EmitPragma,

    /// We have encountered a fatal error and must emit that forever
    Fatal(Diagnostic),

    /// We have consumed all the inpu
    Done,
}

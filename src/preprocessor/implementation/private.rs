//! Private implementation details of the `Preprocessor`

use super::{Preprocessor, State};
use crate::preprocessor::{
    macros,
    output::{Diagnostic, ErrorKind, Item, Severity, Token, TokenKind},
    pragmas::{self, PragmaContext},
};
use std::path::{Path, PathBuf};

/*
 * Initialization code
 */

impl<'s> Preprocessor<'s> {
    pub(super) fn add_pragmas(&mut self) {
        self.register_pragma("once", pragmas::Once::default())
    }

    pub(super) fn add_common_macros(&mut self) {
        self.add_macro_unchecked("__LINE__", macros::standard::LineMacro);
        self.add_macro_unchecked("__FILE__", macros::standard::FileMacro);
        self.add_macro_unchecked("__TIME__", macros::standard::TimeMacro);
        self.add_macro_unchecked("__DATE__", macros::standard::DateMacro);
        self.add_macro_unchecked("__COUNTER__", macros::common::CounterMacro::default());
    }
}

/*
 * General utilities
 */

impl<'s> Preprocessor<'s> {
    pub(super) fn find_system_include(&self, path: &Path) -> Option<PathBuf> {
        self.system_include_paths
            .iter()
            .chain(self.local_include_paths.iter())
            .find_map(|dir| {
                let candidate = dir.join(path);

                if candidate.exists() {
                    Some(candidate)
                } else {
                    None
                }
            })
    }

    pub(super) fn find_local_include(&self, path: &Path) -> Option<PathBuf> {
        self.local_include_paths.iter().find_map(|dir| {
            let candidate = dir.join(path);

            if candidate.exists() {
                Some(candidate)
            } else {
                None
            }
        })
    }
}

/*
 * State machine utilities
 */

impl<'s> Preprocessor<'s> {
    /// Store an Item to be emitted later.
    ///
    /// If it turns out that there was an error or some other reason the Item
    /// shouldn't be emitted an operation like [`Self::discard_tokens()`]
    /// can be used.
    pub(super) fn stage<I: Into<Item<'s>>>(&mut self, item: I) {
        self.staged_emit.push_back(item.into())
    }

    /// Commit all Items currently staged
    ///
    /// Items must have been previously staged with [`Self::stage()`]
    pub(super) fn commit(&mut self) {
        self.emit_queue.append(&mut self.staged_emit)
    }

    /// Immediatly commit an Item without staging
    ///
    /// # Panics
    /// This should not be used when other items are staged
    pub(super) fn commit_immediate<I: Into<Item<'s>>>(&mut self, item: I) {
        debug_assert!(self.staged_emit.is_empty());
        self.emit_queue.push_back(item.into());
    }

    /// Filter out any non-error Items from the staged Items
    pub(super) fn discard_tokens(&mut self) {
        self.staged_emit.retain(|t| matches!(t, &Item::Error(_)));
    }

    /// Immediately transition to the fatal state, committing any Items
    /// currently staged.
    pub(super) fn fatal(&mut self, error: Diagnostic) {
        debug_assert!(error.severity == Severity::Fatal);

        // Commit any queued tokens
        self.commit();
        // Lock ourselves into the fatal state
        self.state = State::Fatal(error);
    }
}

/*
 * State machine processing
 */
impl<'s> Preprocessor<'s> {
    pub(super) fn handle_line_start(&mut self) {
        // Consume tokens unti we find a non-error, non-whitespace item
        let first = loop {
            match self.stack.get() {
                Err(
                    e @ Diagnostic {
                        severity: Severity::Fatal,
                        ..
                    },
                ) => {
                    // Emit a fatal error and stop processing this line
                    self.fatal(e);
                    return;
                }
                Err(e) => self.stage(Item::Error(e)),
                Ok(
                    t @ Token {
                        kind: TokenKind::Whitespace,
                        ..
                    },
                ) => self.stage(Item::Token(t)),
                Ok(t) => break t,
            }
        };

        // Check if this could possibly be a directive
        match first {
            // This is a directive line so try to handle that directive
            // anything we "peeked" up to this point is discarded except the
            // "#" just in case we need to emit a pragma as tokens
            t @ Token {
                kind: TokenKind::Punctuator,
                ..
            } if t.text == "#" => {
                self.stage(Item::Token(t));
                self.do_directive();
            }
            other => {
                // Dump all the peeked tokens and mark for emit
                self.commit();
                // Push the item we got back into the stream because we might
                // have to process it as a macro in the NormalLine state
                self.stack.submit_token(Ok(other));
                self.state = State::NormalLine;
            }
        }
    }

    pub(super) fn handle_normal_line(&mut self) {
        match self.stack.get() {
            Err(
                e @ Diagnostic {
                    severity: Severity::Fatal,
                    ..
                },
            ) => {
                self.fatal(e);
            }

            Err(e) => {
                self.commit_immediate(e);
            }

            // End of the line go back to start of the line
            Ok(
                t @ Token {
                    kind: TokenKind::EOF,
                    ..
                },
            )
            | Ok(
                t @ Token {
                    kind: TokenKind::EOL,
                    ..
                },
            ) => {
                self.commit_immediate(t);
                self.state = State::LineStart;
            }

            // If the token is an identifier we should check if it can be a
            // macro invocation
            Ok(
                t @ Token {
                    kind: TokenKind::Identifier,
                    ..
                },
            ) => match self.macros.get::<str>(&t.text) {
                Some(_macro_code) => unimplemented!(),
                None => self.commit_immediate(t),
            },

            Ok(tok) => {
                self.commit_immediate(tok);
            }
        }
    }

    pub(super) fn handle_emit_pragma(&mut self) {
        match self.stack.get() {
            Err(
                e @ Diagnostic {
                    severity: Severity::Fatal,
                    ..
                },
            ) => {
                self.state = State::Fatal(e);
            }

            Err(e) => {
                self.stage(Item::Error(e));
                self.commit();
            }

            // End of the line go back to normal operation
            Ok(
                t @ Token {
                    kind: TokenKind::EOF,
                    ..
                },
            )
            | Ok(
                t @ Token {
                    kind: TokenKind::EOL,
                    ..
                },
            ) => {
                self.stage(Item::Token(t));
                self.commit();
                self.state = State::LineStart;
            }

            Ok(tok) => {
                self.stage(Item::Token(tok));
                self.commit();
            }
        }
    }
}

/*
 * Directive handling
 */

impl<'s> Preprocessor<'s> {
    pub(super) fn do_directive(&mut self) {
        // Consume errors and whitespace until we find an identifier or number
        let directive_name = loop {
            match self.stack.get() {
                Err(
                    e @ Diagnostic {
                        severity: Severity::Fatal,
                        ..
                    },
                ) => {
                    // Fatal error submit the tokens for emit then
                    // fall in to the fatal state forever
                    self.commit();
                    self.state = State::Fatal(e);
                    return;
                }
                Err(e) => self.stage(Item::Error(e)),
                Ok(
                    t @ Token {
                        kind: TokenKind::Identifier,
                        ..
                    },
                )
                | Ok(
                    t @ Token {
                        kind: TokenKind::PPNumber,
                        ..
                    },
                ) => break t,
                Ok(
                    t @ Token {
                        kind: TokenKind::Whitespace,
                        ..
                    },
                ) => self.stage(Item::Token(t)),
                // TODO: How to handle error case
                Ok(_) => panic!("Invalid preprocessor directive"),
            }
        };

        if directive_name.kind == TokenKind::Identifier {
            match directive_name.text.as_ref() {
                "pragma" => {
                    self.stage(Item::Token(directive_name));
                    self.do_pragma();
                }

                "define" => {
                    self.do_define();
                }

                "undef" => {
                    self.do_undef();
                }

                "include" => {
                    self.do_include(directive_name);
                }

                _ => panic!("Unknown directive name"),
            }
        } else if directive_name.kind == TokenKind::PPNumber {
            todo!("Line directives");
        } else {
            panic!("Expected an identifier or a line-number");
        }
    }

    pub(super) fn do_pragma(&mut self) {
        // Try to find the pragma name
        let pragma_name = loop {
            match self.stack.get() {
                Err(e) => {
                    if e.severity == Severity::Fatal {
                        self.commit();
                        self.state = State::Fatal(e);
                        return;
                    }
                }
                Ok(tok) => {
                    if tok.kind == TokenKind::Whitespace {
                        self.stage(Item::Token(tok));
                    } else if tok.kind == TokenKind::Identifier {
                        break tok;
                    } else {
                        panic!("Expected an Identifier")
                    }
                }
            }
        };

        match self.pragmas.get_mut(pragma_name.text.as_ref()) {
            Some(pragma) => {
                let ctx = PragmaContext {
                    restricted_macro_names: &mut self.restricted_macro_names,
                    are_tokens_supressed: false,
                    suppress_token_output: self
                        .pragma_supress_tokens
                        .get_mut(pragma_name.text.as_ref())
                        .unwrap(),
                    macros: &mut self.macros,
                };

                // Clear items we don't want to emit
                self.staged_emit.clear();

                // Process the pragma, this can consume up-to but not including
                // the next EOF/EOL or fatal error
                let response = pragma.process(ctx, pragma_name, self.stack.line());

                // Submit any items from the pragma
                for item in response.into_iter() {
                    self.stage(item);
                }

                // Consume the rest of the line up to the EOF/EOL or Fatal error
                self.stack.line().count();

                // Get the end of the line
                match self.stack.get() {
                    // Fatal lexer error
                    Err(e) => {
                        self.commit();
                        self.state = State::Fatal(e);
                    }

                    // End of line or file
                    Ok(t) => {
                        self.stage(Item::Token(t));
                        self.commit();
                        self.state = State::LineStart;
                    }
                }
            }
            None if self.produce_pragma_items => {
                // Try to potentially consume a whitespace token
                match self.stack.get() {
                    Ok(tok) if tok.kind == TokenKind::Whitespace => {
                        // Consume we will never emit this
                    }
                    // Otherwise return the item
                    x => self.stack.submit_token(x),
                }

                // Only keep the errors up to this point
                self.discard_tokens();

                let mut args = Vec::new();

                // Consume the rest of the line as arguments for the `Item::Pragma`
                for res in self.stack.line() {
                    match res {
                        Err(e) => self.staged_emit.push_back(Item::Error(e)),
                        Ok(t) => args.push(t),
                    }
                }

                // Because the `Line` ended this can either be EOL/EOF or Fatal
                match self.stack.get() {
                    // Fatal error
                    Err(e) => {
                        self.commit();
                        self.state = State::Fatal(e);
                    }
                    Ok(eol_f) => {
                        self.stage(Item::Pragma {
                            name: pragma_name,
                            args,
                        });
                        self.stage(Item::Token(eol_f));
                        self.commit();
                        self.state = State::LineStart;
                    }
                }
            }
            None => {
                self.stage(Item::Token(pragma_name));
                self.commit();
                self.state = State::EmitPragma;
            }
        }
    }

    pub(super) fn do_define(&mut self) {
        unimplemented!()
    }

    pub(super) fn do_undef(&mut self) {
        unimplemented!()
    }

    pub(super) fn do_include(&mut self, directive: Token<'s>) {
        self.stack.set_header_name_allowed(true);

        let args: Vec<_> = self
            .stack
            .line()
            .filter_map(|res| match res {
                Err(e) => {
                    // Stage errors while processing
                    self.staged_emit.push_back(Item::Error(e));
                    None
                }
                Ok(Token {
                    kind: TokenKind::Whitespace,
                    ..
                }) => None,
                Ok(x) => Some(x),
            })
            .collect();

        match args.as_slice() {
            [include_file, rest @ ..] => {
                // Check if there were too many arguments and emit an error
                if !rest.is_empty() {
                    self.stage(Diagnostic {
                        kind: ErrorKind::UnexpectedArguments {
                            found: rest.len(),
                            expected: 1,
                        },
                        severity: Severity::Error,
                        trace: directive.trace.clone(),
                    });
                }

                let is_system;

                let raw_path = if include_file
                    .text
                    .chars()
                    .next()
                    .expect("ICE: Include header token was empty")
                    == '"'
                {
                    is_system = false;
                    PathBuf::from(include_file.text.trim_matches('"'))
                } else {
                    is_system = true;
                    PathBuf::from(include_file.text.trim_matches('<'))
                };

                let final_path = if is_system {
                    self.find_system_include(&raw_path)
                } else {
                    self.find_local_include(&raw_path)
                };

                // Check that we got a valid path
                let final_path = if let Some(final_path) = final_path {
                    final_path
                } else {
                    self.stage(Diagnostic {
                        kind: ErrorKind::IncludeFileNotFound { path: raw_path },
                        severity: Severity::Fatal,
                        trace: directive.trace,
                    });
                    return;
                };

                let (id, view) = match self.sources.add_file_source(final_path) {
                    Ok(x) => x,
                    Err(kind) => {
                        // Any error while attempting to open or pre-process
                        // a new source file is fatal to compilation
                        self.fatal(Diagnostic {
                            kind,
                            severity: Severity::Fatal,
                            trace: directive.trace,
                        });
                        return;
                    }
                };

                match self.stack.push_file(id, view) {
                    Ok(()) => {}
                    Err(kind) => {
                        // The only error a push can produce is "recursion-detected"
                        self.fatal(Diagnostic {
                            kind,
                            severity: Severity::Fatal,
                            trace: directive.trace,
                        })
                    }
                }

                todo!("Add the new file to the stack");
            }

            [] => self.stage(Diagnostic {
                kind: ErrorKind::UnexpectedArguments {
                    found: 0,
                    expected: 1,
                },
                severity: Severity::Error,
                trace: directive.trace,
            }),
        }
    }
}

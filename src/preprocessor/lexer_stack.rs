use crate::lexer::{ErrorKind as LexerError, Lexer};
use crate::preprocessor::{
    output::{Diagnostic, ErrorKind, Severity, Token, TokenKind, Trace},
    source_handling::{SourceId, SourceView},
};
use std::collections::VecDeque;

pub struct LexerState<'s> {
    pub id: SourceId,
    pub lexer: Lexer<'s, SourceView<'s>>,
    pub queue: VecDeque<Result<Token<'s>, Diagnostic>>,
}

/// A Stack of lexers across multiple files
pub struct LexerStack<'s> {
    fatal_error: bool,

    /// ID of the current lexer
    id: SourceId,

    /// Active lexer
    lexer: Lexer<'s, SourceView<'s>>,

    /// Stores suspended lexers along with their enqueued tokens
    stack: Vec<LexerState<'s>>,

    /// Unprocessed tokens waiting to be viewed
    ///
    /// This can be used internally if we had to peek and see if an error was
    /// fatal, or can be used to submit extra unprocessed tokens from macro
    /// expansion.
    queue: VecDeque<Result<Token<'s>, Diagnostic>>,
}

impl<'s> LexerStack<'s> {
    pub fn new(id: SourceId, view: SourceView<'s>) -> LexerStack<'s> {
        LexerStack {
            fatal_error: true,
            id,
            lexer: Lexer::new(view),

            stack: Vec::with_capacity(5),

            queue: VecDeque::with_capacity(5),
        }
    }

    pub fn get(&mut self) -> Result<Token<'s>, Diagnostic> {
        self.queue.pop_front().unwrap_or_else(|| self.inner_get())
    }

    pub fn current_file_id(&self) -> (usize, SourceId) {
        (self.stack.len(), self.id)
    }

    pub fn submit_token(&mut self, item: Result<Token<'s>, Diagnostic>) {
        self.queue.push_front(item);
    }

    pub fn submit_tokens(&mut self, items: Vec<Result<Token<'s>, Diagnostic>>) {
        self.queue.reserve(items.len());
        for item in items.into_iter().rev() {
            self.queue.push_front(item);
        }
    }

    pub fn set_header_name_allowed(&mut self, is_allowed: bool) {
        self.lexer.set_header_name_allowed(is_allowed)
    }

    pub fn line<'a>(&'a mut self) -> Line<'a, 's> {
        Line {
            done: false,
            stack: self,
        }
    }

    pub fn push_file(&mut self, id: SourceId, view: SourceView<'s>) -> Result<(), ErrorKind> {
        // TODO: Implement recursion detection

        // Replace all the current information with the new lexer file information
        // and push the old lexer state onto the stack
        self.stack.push(LexerState {
            id: std::mem::replace(&mut self.id, id),
            lexer: std::mem::replace(&mut self.lexer, Lexer::new(view)),
            queue: std::mem::replace(&mut self.queue, VecDeque::with_capacity(5)),
        });

        Ok(())
    }

    fn inner_get(&mut self) -> Result<Token<'s>, Diagnostic> {
        loop {
            match self.lexer.next() {
                Some(Ok(tok)) => {
                    return Ok(Token {
                        kind: tok.kind,
                        text: tok.text,
                        trace: Trace::Source {
                            id: self.id,
                            span: tok.span,
                        },
                    })
                }
                Some(Err(e)) => match self.lexer.next() {
                    // If the next error is a recovery failure then emit this error as fatal
                    Some(Err(LexerError::CannotRecoverFromError)) => {
                        self.fatal_error = true;
                        return Err(Diagnostic {
                            kind: ErrorKind::LexerError(e),
                            severity: Severity::Fatal,
                            trace: Trace::Unknown,
                        });
                    }

                    // Store the next character in a peek
                    Some(Ok(tok)) => {
                        self.queue.push_back(Ok(Token {
                            kind: tok.kind,
                            text: tok.text,
                            trace: Trace::Source {
                                id: self.id,
                                span: tok.span,
                            },
                        }));

                        return Err(Diagnostic {
                            kind: ErrorKind::LexerError(e),
                            severity: Severity::Error,
                            trace: Trace::Unknown,
                        });
                    }

                    Some(Err(e2)) => {
                        self.queue.push_back(Err(Diagnostic {
                            kind: ErrorKind::LexerError(e2),
                            severity: Severity::Error,
                            trace: Trace::Unknown,
                        }));

                        return Err(Diagnostic {
                            kind: ErrorKind::LexerError(e),
                            severity: Severity::Error,
                            trace: Trace::Unknown,
                        });
                    }

                    None => {}
                },
                None => {}
            }

            // The queue should be empty or we shouldn't be in this state
            debug_assert!(
                self.queue.is_empty(),
                "Lexer stack performed an inner get while Tokens were still in the peek-queue"
            );

            // This item returned `None` so pop it and move on to the next
            let LexerState { id, lexer, queue } =
                self.stack.pop().expect("ICE: No more items on the stack");
            self.id = id;
            self.lexer = lexer;
            self.queue = queue;
        }
    }
}

impl<'s> Iterator for LexerStack<'s> {
    type Item = Result<Token<'s>, Diagnostic>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.get())
    }
}

/// Provides access to one line of tokens from the underlying lexer
/// Up to an EOL/EOF or Fatal Error, but does not emit the final token
pub struct Line<'a, 's> {
    done: bool,
    stack: &'a mut LexerStack<'s>,
}

impl<'a, 's> Line<'a, 's> {
    pub fn current_file_id(&self) -> (usize, SourceId) {
        self.stack.current_file_id()
    }
}

impl<'a, 's> Iterator for Line<'a, 's> {
    type Item = Result<Token<'s>, Diagnostic>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            None
        } else {
            match self.stack.get() {
                e @ Err(Diagnostic {
                    severity: Severity::Fatal,
                    ..
                }) => {
                    self.done = true;
                    self.stack.submit_token(e);
                    None
                }
                e @ Err(_) => Some(e),
                t @ Ok(Token {
                    kind: TokenKind::EOL,
                    ..
                })
                | t @ Ok(Token {
                    kind: TokenKind::EOF,
                    ..
                }) => {
                    self.done = true;
                    self.stack.submit_token(t);
                    None
                }
                t @ Ok(_) => Some(t),
            }
        }
    }
}

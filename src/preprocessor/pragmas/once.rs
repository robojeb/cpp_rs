use super::{Line, Pragma, PragmaContext};
use crate::preprocessor::{
    output::{Diagnostic, ErrorKind, Item, Severity, Token, TokenKind, Trace},
    source_handling::SourceId,
};
use std::collections::HashSet;

/// A preprocessor pragma which prevents a file from being compiled more than
/// once.
///
/// # Notes on use
/// Unlike other implementations of `#pragma once` this version only applies
/// to tokens which occur after the directive is found.
///
/// This limitation exists because of the interactive iterator nature of this
/// implementation of the C-preprocessor.
/// As a workaround, the pre-processor has a setting `Preprocessor::prescan_pragma_once`
/// which will scan a source file for a `#pragma once` directive and if triggered
/// will prevent any tokens from that file being read.
#[derive(Debug, Default)]
pub struct Once {
    seen_files: HashSet<SourceId>,
    seen_in_file: HashSet<(usize, SourceId)>,
}

impl<'s> Pragma<'s> for Once {
    fn process<'a>(
        &mut self,
        ctx: PragmaContext<'a, 's>,
        name: Token<'s>,
        args: Line<'a, 's>,
    ) -> Vec<Item<'s>> {
        let id_tuple = args.current_file_id();
        let (_, src_id) = args.current_file_id();

        // Make sure we haven't already seen `#pragma once` in this file
        // already
        if self.seen_in_file.contains(&id_tuple) {
            return Vec::new();
        }
        self.seen_in_file.insert(id_tuple);

        let extra_args = args
            .filter(|res| {
                !matches!(
                    res,
                    Err(_)
                        | &Ok(Token {
                            kind: TokenKind::Whitespace,
                            ..
                        })
                )
            })
            .count();

        if self.seen_files.contains(&src_id) {
            // We assume that `#import` directives won't happen if we have
            // suppressed the output so we don't track a stack of these items
            *ctx.suppress_token_output = true;
        } else {
            self.seen_files.insert(src_id);
        }

        if extra_args != 0 {
            vec![Item::Error(Diagnostic {
                kind: ErrorKind::UnexpectedArguments {
                    expected: 0,
                    found: extra_args,
                },
                severity: Severity::Warning,
                trace: Trace::WhileProcessingDirective(Box::new(name.trace)),
            })]
        } else {
            Vec::new()
        }
    }

    fn on_eof<'a>(&mut self, _eof: &Token<'s>, id: (usize, SourceId), ctx: PragmaContext<'a, 's>) {
        // Stop supressing because the file has ended
        *ctx.suppress_token_output = false;
        self.seen_in_file.remove(&id);
    }
}

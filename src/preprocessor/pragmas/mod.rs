mod common;
mod once;

pub use super::lexer_stack::Line;
use super::macros::Macro;
use super::source_handling::SourceId;
use crate::preprocessor::output::{Item, Token};
pub use once::Once;
use std::collections::{HashMap, HashSet};

/// Preprocessor flags that pragmas can modify
pub struct PragmaContext<'a, 's> {
    /// What macro names are not allowed to be overwritten or undefined
    pub restricted_macro_names: &'a mut HashSet<String>,

    /// Are tokens currently being supressed by one or more other pragmas
    ///
    /// This is useful in case a pragma should not operate if it would have been
    /// supressed (eg a push-pop macro pragma)
    pub are_tokens_supressed: bool,

    /// Indicates if this pragma thinks that further tokens should be suppressed
    ///
    /// Each Pragma (and the `#if` family of directives) have their own copy of this
    /// flag, and if any are `true` tokens will be supressed.
    pub suppress_token_output: &'a mut bool,

    /// Currently defined macros
    pub macros: &'a mut HashMap<String, Box<dyn Macro<'s> + 's>>,
}

/// A Trait to define a new pragma which affects the preprocessor state
pub trait Pragma<'s> {
    fn process<'a>(
        &mut self,
        ctx: PragmaContext<'a, 's>,
        name: Token<'s>,
        args: Line<'a, 's>,
    ) -> Vec<Item<'s>>;

    fn on_eof<'a>(&mut self, eof: &Token<'s>, id: (usize, SourceId), ctx: PragmaContext<'a, 's>);
}

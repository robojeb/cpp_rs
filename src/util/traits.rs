use super::{CharInfo, IdxInfo};
use std::borrow::Cow;

pub trait CharSource<'s> {
    /// Get the next character from the source
    ///
    /// Along with the actual `char` value this will return information about the
    /// byte the character starts in the stream, and the index of the character.
    /// These values may differ when working with non-ASCII UTF-8 encoded text.
    fn next(&mut self) -> Option<CharInfo>;
    /// Extract a slice from the source using the provided indexing information
    fn get_slice(&mut self, start: IdxInfo, end: IdxInfo) -> Cow<'s, str>;
    /// Get the number of bytes in the source
    fn get_byte_len(&self) -> usize;
    /// Get the number of characters in the source
    fn get_char_len(&self) -> usize;
}

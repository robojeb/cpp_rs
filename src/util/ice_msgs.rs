//! Contains all the ICE messages that can be produced by the crate
//!

macro_rules! ices {
    ($($(#[$doc:meta])*const $name:ident = $text:literal;)*) => {
        $(
            $(#[$doc])*
            pub const $name: &str = concat!("[ICE]: ", $text);
        )*
    }
}

ices! {
    /// Lock poisoning on Sources internal state
    const ICE_SOURCES_POISON = "Could not lock sources internal state due to poisoning.";
}

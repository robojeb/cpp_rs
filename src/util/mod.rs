pub mod char_sources;
pub mod ice_msgs;
pub mod traits;

use std::ops::RangeInclusive;

/// A struct for the output of the lexer or preprocessor
///
/// When run in a "one-shot", rather than iterative, mode the lexer or preprocessor
/// will return this `Output` struct with their specific Token (T) and Error (E)
/// types.
///
/// In the case of an unrecoverable error the `successful` flag will be set to
/// `false` and the last error in the `errors` vector will be the unrecoverable
/// error.
pub struct Output<T, E> {
    pub successful: bool,
    pub tokens: Vec<T>,
    pub errors: Vec<E>,
}

/// Information about the range of bytes, characters, and lines in a source file
/// that a token spans.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Span {
    /// The bytes from the source this token covers
    pub bytes: RangeInclusive<usize>,
    /// The characters from the source that this token contains
    pub chars: RangeInclusive<usize>,
    /// The lines from the source that this token spans
    pub lines: RangeInclusive<usize>,
}

pub struct Spanned<T> {
    pub span: Span,
    pub item: T,
}

/// Indexing information for a single character from a `traits::CharSource` type
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IdxInfo {
    pub byte: usize,
    pub char_index: usize,
}

/// A Single character from a source along with its associated index information
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CharInfo {
    /// The character in the stream
    pub char: char,
    /// Indexing information for the character
    pub idx: IdxInfo,
}

/// Used to inform the preprocessor which standard of the C++ standard you expect
/// the `__cplusplus` macro to expand to.
pub enum CppStd {
    Cpp98,
    Cpp11,
    Cpp14,
    Cpp17,
    Cpp20,
    Other(String),
}

impl CppStd {
    pub(crate) fn get_expand(self) -> String {
        match self {
            CppStd::Cpp98 => "199711L".into(),
            CppStd::Cpp11 => "201103L".into(),
            CppStd::Cpp14 => "201402L".into(),
            CppStd::Cpp17 => "201703L".into(),
            CppStd::Cpp20 => "202002L".into(),
            CppStd::Other(s) => s,
        }
    }
}

pub enum CStd {
    C89,
    C99,
    C11,
    C17,
    Other(String),
}

impl CStd {
    pub(crate) fn get_expand(self) -> String {
        match self {
            CStd::C89 => "199409L".into(),
            CStd::C99 => "199901L".into(),
            CStd::C11 => "201112L".into(),
            CStd::C17 => "201710L".into(),
            CStd::Other(s) => s,
        }
    }
}

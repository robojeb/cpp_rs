use super::{traits::CharSource, CharInfo, IdxInfo};
use std::{
    borrow::Cow,
    iter::{Enumerate, Fuse},
    str::CharIndices,
};

pub struct StrSource<'s> {
    input: &'s str,
    char_indices: Fuse<Enumerate<CharIndices<'s>>>,
}

impl<'s> CharSource<'s> for StrSource<'s> {
    fn next(&mut self) -> Option<CharInfo> {
        self.char_indices
            .next()
            .map(|(index, (byte, char))| CharInfo {
                char,
                idx: IdxInfo {
                    byte,
                    char_index: index,
                },
            })
    }

    fn get_slice(&mut self, start: IdxInfo, end: IdxInfo) -> Cow<'s, str> {
        Cow::Borrowed(
            self.input
                .get(start.byte..=end.byte)
                .expect("Index was invalid, either out of bounds or not on a UTF-8 boundary"),
        )
    }

    fn get_byte_len(&self) -> usize {
        self.input.len()
    }

    fn get_char_len(&self) -> usize {
        self.input.chars().count()
    }
}

impl<'s> From<&'s str> for StrSource<'s> {
    fn from(other: &'s str) -> Self {
        StrSource {
            char_indices: other.char_indices().enumerate().fuse(),
            input: other,
        }
    }
}

pub struct StringSource {
    char_len: usize,
    cur_char: usize,
    cur_byte: usize,
    data: String,
}

impl CharSource<'static> for StringSource {
    fn next(&mut self) -> Option<CharInfo> {
        if let Some(char) = self.data[self.cur_byte..].chars().next() {
            let width = char.len_utf8();

            let idx = self.cur_char;
            let byte = self.cur_byte;

            self.cur_char += 1;
            self.cur_byte += width;

            Some(CharInfo {
                char,
                idx: IdxInfo {
                    byte,
                    char_index: idx,
                },
            })
        } else {
            None
        }
    }

    fn get_slice(&mut self, start: IdxInfo, end: IdxInfo) -> Cow<'static, str> {
        Cow::Owned(
            self.data
                .get(start.byte..=end.byte)
                .expect("Index was invalid, either out of bounds or not on a UTF-8 boundary")
                .into(),
        )
    }

    fn get_byte_len(&self) -> usize {
        self.data.len()
    }

    fn get_char_len(&self) -> usize {
        self.char_len
    }
}

impl From<String> for StringSource {
    fn from(data: String) -> Self {
        StringSource {
            char_len: data.chars().count(),
            cur_byte: 0,
            cur_char: 0,
            data,
        }
    }
}

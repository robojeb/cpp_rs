use regex::{Captures, Regex};

use crate::util::Span;
use once_cell::sync::Lazy;
use std::borrow::{Borrow, Cow};

static UCN_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r#"\\u([0-9a-fA-F]{4})|\\U([0-9a-fA-F]{8})"#)
        .expect("ICE: Failed to compile static Regex")
});

/// Indicates the proper interpretation of a [Token].
///
/// Every character (except escaped newlines) will be turned into a token of
/// one of the following types.
///
//[c11]: S6.4 Lexical elements
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum TokenKind {
    /// An identifier
    Identifier,
    /// A Preprocessor number
    PPNumber,
    /// A Quoted or Angle-braced string which references a header
    ///
    /// # Token Text
    /// Any token of this kind produced by the Lexer will have a matching pair
    /// of `""` or `<>` characters at the start and end of the [Token::text]
    /// field.
    HeaderName,
    /// A single character constant
    CharacterConstant,
    /// A quoted string
    ///
    /// # Token Text
    /// Any token of this kind produced by the Lexer will have a quote (`"`) at
    /// the start and end of the [Token::text] field.
    StringLiteral,
    /// A single punctuator
    Punctuator,
    /// Non-newline whitespace token
    Whitespace,
    /// A line-ending character.
    EOL,
    /// A token that indicates we reached the end of the file
    ///
    /// # Token Text
    /// The [Token::text] field of this token will be empty.
    EOF,
    /// A token that indicates a comment
    ///
    /// # Token Text
    /// A comment which starts with `/*` will have a matching `*/` at the end
    /// of the [Token::text] field. Comments starting with `//` will not have
    /// a newline as that will be emitted as a separate token.
    Comment,
}

/// A Token produced by the lexer
///
/// Each Token will be marked with a [TokenKind] which determines how the
/// [Token::text] field should be interpreted. The [Lexer] will perform
/// minimal transformation of the Token text and where possible will emit
/// a text borrowed from the Lexer input.
///
/// This means that the text field of a token can contain escape sequences
/// like universal character names or hex character escapes.
/// The Token struct provides two functions to get interpretations of the
///
/// Unlike `crate::preprocessor::Token` a lexer token has no concept of what
/// source file it came from.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Token<'s> {
    pub kind: TokenKind,
    pub span: Span,
    pub text: Cow<'s, str>,
}

impl<'s> Token<'s> {
    /// Get the raw text as it appears in the source file
    pub fn raw_text(&self) -> &'_ str {
        self.text.borrow()
    }

    /// Get the text after resolving all escape sequences
    ///
    /// This includes expansion of all universal-character-names, hex and octal
    /// escape sequences, and digraphs.
    pub fn escaped_text(&self) -> Cow<'_, str> {
        match self.kind {
            TokenKind::Identifier => {
                UCN_REGEX.replace_all(self.text.borrow(), |caps: &Captures| -> String {
                    let value = char::from_u32(u32::from_str_radix(&caps[0], 16).unwrap()).unwrap();

                    value.into()
                })
            }
            TokenKind::StringLiteral => unimplemented!(),
            TokenKind::CharacterConstant => unimplemented!(),
            TokenKind::Whitespace | TokenKind::Comment => Cow::Borrowed(" "),
            TokenKind::EOL => Cow::Borrowed("\n"),
            TokenKind::EOF => unimplemented!(),
            TokenKind::HeaderName | TokenKind::PPNumber => Cow::Borrowed(self.text.borrow()),
            TokenKind::Punctuator => match self.text.borrow() {
                "<:" => Cow::Borrowed("["),
                ":>" => Cow::Borrowed("]"),
                "<%" => Cow::Borrowed("{"),
                "%>" => Cow::Borrowed("}"),
                "%:" => Cow::Borrowed("#"),
                "%:%:" => Cow::Borrowed("##"),
                _ => Cow::Borrowed(self.text.borrow()),
            },
        }
    }

    /// Get the `text` of the Token in a format valid for existing within
    /// a string literal.
    ///
    /// The output produced should not contain the surrounding quote `"`
    /// characters.  
    pub fn string_encoded_text(&self) -> Cow<'s, str> {
        unimplemented!()
    }
}

fn decode_ucn<'a>(value: Captures<'a>) -> String {
    unimplemented!()
}

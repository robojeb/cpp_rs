mod private;
mod public;

use crate::util::traits::CharSource;

use super::peek_adapter::PeekAdapter;
use crate::lexer::{ErrorKind, Token};

use private::State;
use std::collections::VecDeque;

// /// A variant of the `try!` macro which is designed to handle the various
// /// state machine actions. This works just like `try!` but propagates both
// /// Err and Token variants.
// macro_rules! try_action {
//     ($e:expr) => {
//         match $e {
//             ActionResult::Ok(good) => good,
//             ActionResult::Err(e) => return ControlFlow::Break(Err(e)),
//             ActionResult::Token(t) => return ControlFlow::Break(Ok(t)),
//         }
//     };
// }

/// The Lexer state-machine
///
///
#[derive(Debug)]
pub struct Lexer<'s, S: CharSource<'s>> {
    state: State,

    is_header_valid: bool,
    hit_eof: bool,

    /// Checker to see if a character is part of the valid set of identifier
    /// characters
    ext_char_check: Option<fn(char) -> bool>,

    /// The char stream handles all the Multi-peeking grabbing of indices
    char_input: PeekAdapter<S>,

    emit_queue: VecDeque<Result<Token<'s>, ErrorKind>>,
}

/// Allow the lexer to work as an iterator
///
/// This implementation returns items as `Result<Token<'a>, Error>`
/// This allows the nice transformation from `Vec<Result<Token<'a>, Error>>` to
/// `Result<Vec<Token<'a>>, Error>` using `Iterator::collect()` which allows
/// easy early termination upon an error if desired.
#[cfg_attr(tarpaulin, skip)]
impl<'a, S: CharSource<'a>> Iterator for Lexer<'a, S> {
    type Item = Result<Token<'a>, ErrorKind>;
    /// Iterates over all the tokens in the stream. The iterator will return
    /// `None` after either `EOF` or an `UnrecoverableError` is produced.
    fn next(&mut self) -> Option<Self::Item> {
        if self.hit_eof {
            None
        } else {
            Some(self.get_next_token())
        }
    }
}

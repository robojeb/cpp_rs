mod headers;

use super::Lexer;
use crate::lexer::{ErrorKind, Token, TokenKind};
use crate::util::{traits::CharSource, CharInfo, Span};
use log::trace;
use std::borrow::Cow;
use std::ops::ControlFlow;

#[derive(Debug, PartialEq, Eq, Clone)]
pub(super) enum State {
    Start,

    Whitespace,
    EoL,

    LineComment,
    BlockComment,
    DivideOrComment,

    QuotedHeaderName,
    BracketedHeaderName,

    IdentOrString,
    IdentOrStringU8,
    StringLiteral,

    Identifier,
    Ucn {
        to_count: usize,
        return_to: Box<State>,
    },

    PPNumberOrDot,
    PPNumber,
    PPNumberExponent,

    PunctGTStart,
    PunctLTStart,
    PunctAndEqual,
    PunctPercent,
    PunctColon,
    PunctMinus,
    PunctDouble(char),
    PunctDoubleOrEq(char),
    AltPound,
    Error(Box<State>),
}

impl<'s, S: CharSource<'s>> Lexer<'s, S> {
    pub(in crate::lexer::implementation) fn handle_start(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::EOF)?;

        match next_char {
            // Line endings are `\n` and `\r`
            '\n' => {
                self.transition(State::EoL);
            }

            // Whitespace is anything which isn't a newline
            _ if next_char.is_ascii_whitespace() => {
                self.consume(1)?;
                self.transition(State::Whitespace);
            }

            // [c11]: §6.4 ¶4
            // If we see a quote and are in a valid state for a HeaderName token we transition to the
            // quoted header name state.
            '"' if self.is_header_valid => {
                self.consume(1)?;
                self.transition(State::QuotedHeaderName);
            }

            // [c11]: §6.4 ¶4
            // If we see a bracket and are in a valid state for a HeaderName token we transition to the
            // bracketed header name state.
            '<' if self.is_header_valid => {
                self.consume(1)?;
                self.transition(State::BracketedHeaderName);
            }

            // [c11]: §6.4.5 ¶1
            // If we see any of the following symbols it is possible
            // we are starting a string literal or an identifier.
            'u' | 'U' | 'L' => {
                self.consume(1)?;
                self.transition(State::IdentOrString);
            }

            // [c11]: §6.4.5 ¶1
            // If we start with a quote we are as string literal
            '"' => {
                self.consume(1)?;
                self.transition(State::StringLiteral);
            }

            // [c11]: §6.4.2.1 ¶1
            // An underline can start an identifier
            '_' => {
                self.consume(1)?;
                self.transition(State::Identifier);
            }

            // [c11]: §6.4.2.1 ¶1
            // Any alphabetic character can start an identifier
            _ if next_char.is_ascii_alphabetic() => {
                self.consume(1)?;
                self.transition(State::Identifier);
            }

            // [c11]: §6.4.8 ¶1
            // Anything which starts with a digit is going to be a
            // Preprocessor Number
            _ if next_char.is_ascii_digit() => {
                self.consume(1)?;
                self.transition(State::PPNumber);
            }

            // [c11]: §6.4.8 ¶1
            // This can either be a PPNumber or can be a punctuator
            // based on whichever is going to be longer so we have
            // to check.
            '.' => {
                self.consume(1)?;
                self.transition(State::PPNumberOrDot);
            }

            // [c11]: §6.4.6 ¶1
            // Simple punctuators don't have any follow up characters
            '[' | ']' | '(' | ')' | '{' | '}' | ';' | ',' | '?' => {
                self.consume(1)?;
                return self.emit(TokenKind::Punctuator);
            }

            // [c11]: §6.4.6 ¶1
            // Punctuators which start with '<' this includes
            //  * <
            //  * <=
            //  * <<
            //  * <<=
            //  * <:
            //  * <%
            '<' => {
                self.consume(1)?;
                self.transition(State::PunctLTStart);
            }

            // [c11]: §6.4.6 ¶1
            // Punctuators which start with '>' this includes
            //  * >
            //  * >=
            //  * >>
            //  * >>=
            '>' => {
                self.consume(1)?;
                self.transition(State::PunctGTStart);
            }

            // [c11]: §6.4.6 ¶1
            // Puncuators which start with '%' including:
            //  * %
            //  * %=
            //  * %>
            //  * %:
            //  * %:%:
            '%' => {
                self.consume(1)?;
                self.transition(State::PunctPercent);
            }

            // [c11]: §6.4.6 ¶1
            // Punctuators which start with ':' include:
            //  * :
            //  * :%
            //  * :%:%
            //  * :>
            ':' => {
                self.consume(1)?;
                self.transition(State::PunctColon);
            }

            // [c11]: §6.4.6 ¶1
            // Punctuators which start with '-' include:
            //  * -
            //  * --
            //  * -=
            //  * ->
            '-' => {
                self.consume(1)?;
                self.transition(State::PunctMinus);
            }

            // [c11]: §6.4.6 ¶1
            // Punctuators which only exist on their own as a pair or as part
            // of an assignment operator
            x @ '+' | x @ '|' | x @ '&' => {
                self.consume(1)?;
                self.transition(State::PunctDoubleOrEq(x));
            }

            // [c11]: §6.4.6 ¶1
            // Handle all the punctuators which can be themselves or with an
            // equals only:
            //  * ^ | ^=
            //  * * | *=
            //  * ! | !=
            //  * = | ==
            '^' | '*' | '!' | '=' | '~' => {
                self.consume(1)?;
                self.transition(State::PunctAndEqual);
            }

            // [c11]: §6.4.6 ¶1
            // Handles things which start with a slash
            //  * / | /=
            //  * //
            //  * /*
            '/' => {
                self.consume(1)?;
                self.transition(State::DivideOrComment)
            }

            c @ '#' => {
                self.consume(1)?;
                self.transition(State::PunctDouble(c));
            }
            _ => unimplemented!(),
        }
        ControlFlow::Continue(())
    }

    // When we enter this state we will have consumed one of [u,U,L]
    // which can either start an identifier or can be a string
    // literal encoding-prefix
    //
    // Consumes: ("|8)?
    //
    // On EOF: Emit an Identifier
    //
    pub(in crate::lexer::implementation) fn handle_ident_or_string(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Identifier)?;

        match next_char {
            // If we consume a '"' then we are definitely a string
            // literal.
            '"' => {
                self.consume(1)?;
                self.transition(State::StringLiteral);
            }
            // If we consume an '8' then we could still be consuming
            // an encoding prefix or we could be an identifier (eg
            // "u88")
            '8' => {
                self.consume(1)?;
                self.transition(State::IdentOrStringU8);
            }
            // Otherwise we are an identifier. We will not consume
            // so that all the error checking and next state
            // transitions (eg '+') can be handled by the Identifier
            // state
            _ => {
                self.transition(State::Identifier);
            }
        }

        ControlFlow::Continue(())
    }

    // When we enter this state we will have consumed 'u8' which is
    // either an `encoding-prefix` or the start of an identifier.
    //
    // Consumes: "?
    //
    // On EOF: Emit an Identifier
    //
    pub(in crate::lexer::implementation) fn handle_ident_or_string_u8(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Identifier)?;

        match next_char {
            // If we consume a '"' then we are definitely a string
            // literal.
            '"' => {
                self.consume(1)?;
                self.transition(State::StringLiteral);
            }
            // Otherwise we are an identifier. We will not consume
            // so that all the error checking and next state
            // transitions (eg '+') can be handled by the Identifier
            // state
            _ => {
                self.transition(State::Identifier);
            }
        }
        ControlFlow::Continue(())
    }

    // When we enter this state we will have consumed one or more
    // `identifier-nondigit` characters we can then consume all
    // remaining `identifier-nondigit` and `digit` values.
    //
    // Consumes: ([a-zA-Z0-9_]|\u[0-9a-f]{4}|\U[0-9a-f]{8})*
    //
    // On EOF: Emits an identifier
    //
    // [c11]: §6.4.2.1 ¶1
    pub(in crate::lexer::implementation) fn handle_identifier(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Identifier)?;

        match next_char {
            // This can only begin a UCN so we should peek for the
            // following character. This peek can find an EOF
            // in which case we emit the token and then go back
            // to start because we will treat the next as a newline escape
            '\\' => {
                if let Some(CharInfo { char: esc_char, .. }) = self.try_peek() {
                    match esc_char {
                        // Find a 4 byte UCN
                        'u' => {
                            self.consume(2)?;
                            self.transition(State::Ucn {
                                to_count: 4,
                                return_to: Box::new(State::Identifier),
                            });
                        }
                        // Find an 8 byte UCN
                        'U' => {
                            self.consume(2)?;
                            self.transition(State::Ucn {
                                to_count: 8,
                                return_to: Box::new(State::Identifier),
                            });
                        }
                        c => {
                            self.transition(State::Error(Box::new(State::Identifier)));
                            return ControlFlow::Break(Err(ErrorKind::UnexpectedCharacter {
                                found: c,
                                expected: vec!['u', 'U'],
                            }));
                        }
                    }
                } else {
                    // Because it is `None` we found EOF so emit an
                    // identifier and go back to start
                    self.transition(State::Start);
                    return self.emit(TokenKind::Identifier);
                }
            }

            // Consume an '_' character
            '_' => {
                self.consume(1)?;
            }

            // Consume any digit in [0-9a-zA-Z]
            _ if next_char.is_ascii_alphanumeric() => {
                self.consume(1)?;
            }

            // If the user has provided a set of additional characters that
            // are valid in identifiers then check if this next character should
            // be part of the identifier.
            c if self.ext_char_check.is_some() && self.ext_char_check.unwrap()(c) => {
                self.consume(1)?;
            }

            // We must be at the end of the identifier
            // NOTE: Technically the implementation can define other
            // characters to be allowed in an identifier but we don't
            // do that right now.
            _ => {
                self.transition(State::Start);
                return self.emit(TokenKind::Identifier);
            }
        }

        ControlFlow::Continue(())
    }

    // When we enter this state we will have consumed an optional
    // `encoding-prefix` and an opening '"'. We will continue to
    // consume characters until we find an unescaped close quote.
    //
    // Consumes: (\.|[^\"])*"
    //
    // On EOF: Emit an error about an unterminated string
    //
    // Other Errors: Emit an error if a newline is encountered
    //
    pub(in crate::lexer::implementation) fn handle_string_literal(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_error(ErrorKind::StringLiteralMissingClosingQuote)?;

        match next_char {
            '\n' => {
                // This is recoverable if needed (we can pretend to inject a quote)
                // so set the state to the error state and allow it to recover
                self.transition(State::Error(Box::new(State::StringLiteral)));
                return ControlFlow::Break(Err(ErrorKind::StringLiteralMissingClosingQuote));
            }
            '\\' => {
                // If we have an escape we need to consume another character so we peek
                // again to make sure we don't run off the end of the string
                trace!("Peeking for escape");
                self.peek_or_error(ErrorKind::StringLiteralMissingClosingQuote)?;
                self.consume(2)?;
            }

            // On a closing quote we will emit a string literal and return to start
            '"' => {
                self.consume(1)?;
                self.transition(State::Start);
                return self.emit(TokenKind::StringLiteral);
            }

            // Otherwise we will continue consuming the input characters
            _ => {
                self.consume(1)?;
            }
        }
        ControlFlow::Continue(())
    }

    // When we enter this state we will have consumed a '\u' or '\U'
    // and potentially a number of Hex characters. Unlike other
    // states this doesn't actually emit tokens but will consume a
    // UCN and then return to the state which called it.
    //
    // Consumes: [0-9a-fA-F]{to_count}
    //
    // On EOF: Emit an early EOF error
    //
    // [c11]: §6.4.3 ¶1
    pub(in crate::lexer::implementation) fn handle_ucn(
        &mut self,
        to_count: usize,
        return_to: Box<State>,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        for i in 0..to_count {
            let next_char = self.peek()?;

            if next_char.is_ascii_hexdigit() {
                self.consume(1)?;
            } else {
                self.transition(State::Error(Box::new(State::Ucn {
                    to_count: to_count - i,
                    return_to,
                })));
                return ControlFlow::Break(Err(ErrorKind::UnexpectedCharacter {
                    found: next_char,
                    expected: vec![
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
                        'f', 'A', 'B', 'C', 'D', 'E', 'F',
                    ],
                }));
            }
        }
        self.transition(*return_to);
        ControlFlow::Continue(())
    }

    // When we enter this state we will have to have consumed at
    // least one digit so we can consume anything that is part of a
    // PPNumber
    //
    // Consumes:
    pub(in crate::lexer::implementation) fn handle_ppnumber(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::PPNumber)?;

        match next_char {
            'e' | 'E' | 'p' | 'P' => {
                self.consume(1)?;
                self.transition(State::PPNumberExponent);
            }

            _ if next_char.is_ascii_alphanumeric() => {
                self.consume(1)?;
            }

            '_' | '.' => {
                self.consume(1)?;
            }

            // This can only begin a UCN so we should peek for the
            // following character. This peek can find an EOF
            // in which case we emit the token and then go back
            // to start because we will treat the next as a newline escape
            '\\' => {
                if let Some(CharInfo { char: esc_char, .. }) = self.try_peek() {
                    match esc_char {
                        // Find a 4 byte UCN
                        'u' => {
                            self.consume(2)?;
                            self.transition(State::Ucn {
                                to_count: 4,
                                return_to: Box::new(State::PPNumber),
                            });
                        }
                        // Find an 8 byte UCN
                        'U' => {
                            self.consume(2)?;
                            self.transition(State::Ucn {
                                to_count: 8,
                                return_to: Box::new(State::PPNumber),
                            });
                        }
                        c => {
                            self.transition(State::Error(Box::new(State::PPNumber)));
                            return ControlFlow::Break(Err(ErrorKind::UnexpectedCharacter {
                                found: c,
                                expected: vec!['u', 'U'],
                            }));
                        }
                    }
                } else {
                    // Because it is `None` we found EOF so emit an
                    // identifier and go back to start
                    self.transition(State::Start);
                    return self.emit(TokenKind::PPNumber);
                }
            }

            _ => {
                self.transition(State::Start);
                return self.emit(TokenKind::PPNumber);
            }
        }
        ControlFlow::Continue(())
    }

    // When we enter this state we will have consumed part of a
    // PPNumber and an 'e' | 'E' | 'p' | 'P'. The only time we can
    // consume a '+' or '-' in a PPNumber is after one of these so
    // if we find that we will consume it.
    // Otherwise we will just go back to the PPNumber state
    //
    // Consumes: (+|-)?
    //
    // On EOF: Emit a PPNumber
    //
    pub(in crate::lexer::implementation) fn handle_ppnumber_exponent(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::PPNumber)?;

        match next_char {
            '-' | '+' => {
                self.consume(1)?;
            }
            _ => {}
        }
        self.transition(State::PPNumber);
        ControlFlow::Continue(())
    }

    // When we enter this state we will have seen one '.' and we have
    // to check for either a digit (PPNumber) or two '..' so we will
    // can transfer to a PPNumber or emit a '.' or a '...'
    //
    // Consumes: (..|Digit)?
    //
    // On EOF: Emits a punctuator
    //
    pub(in crate::lexer::implementation) fn handle_ppnumber_or_dot(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        match next_char {
            _ if next_char.is_ascii_digit() => {
                self.consume(1)?;
                self.transition(State::PPNumber);
            }

            '.' => {
                if let Some(CharInfo { char: '.', .. }) = self.try_peek() {
                    self.consume(2)?;
                }
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }

            _ => {
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }
        }
        ControlFlow::Continue(())
    }

    // When we reach this state we will have consumed a '>'. We then
    // search for other symbols which can make a valid punct token
    //
    // Consumes: (> | =)?
    //
    // On Eof: Emits a Punctuator token
    //
    pub(in crate::lexer::implementation) fn handle_punct_gt(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        match next_char {
            '=' => {
                self.consume(1)?;
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }

            '>' => {
                self.consume(1)?;
                self.transition(State::PunctAndEqual);
            }

            _ => {
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }
        }
        ControlFlow::Continue(())
    }

    // When we reach this state we will have consumed a '<'. We then
    // search for other symbols which can make a valid punct token
    //
    // Consumes: (< | = | : | %)?
    //
    // On Eof: Emits a Punctuator token
    //
    pub(in crate::lexer::implementation) fn handle_punct_lt(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        match next_char {
            '=' | ':' | '%' => {
                self.consume(1)?;
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }

            '<' => {
                self.consume(1)?;
                self.transition(State::PunctAndEqual);
            }

            _ => {
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }
        }
        ControlFlow::Continue(())
    }

    // When we reach this state we will have consumed a punctuation character
    // which can also be combined with an '=' to form a valid punctuator.
    // Here we will consume an '=' or emit the token as is.
    //
    // Consumes: =?
    //
    // On Eof: Emits a punctuator
    //
    pub(in crate::lexer::implementation) fn handle_punct_and_equal(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        match next_char {
            '=' => {
                self.consume(1)?;
                self.transition(State::Start);
                self.emit(TokenKind::Punctuator)
            }

            _ => {
                self.transition(State::Start);
                self.emit(TokenKind::Punctuator)
            }
        }
    }

    // When we enter this state we have consumed a '%' so we can consume a number
    // of other tokens to make valid punctuators
    //
    // Consumes: (=| > | :)?
    //
    // On EOF: Emits a punctuator
    //
    pub(in crate::lexer::implementation) fn handle_punct_percent(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        match next_char {
            '=' | '>' => {
                self.consume(1)?;
                self.transition(State::Start);
                self.emit(TokenKind::Punctuator)
            }

            ':' => {
                self.consume(1)?;
                self.transition(State::AltPound);
                ControlFlow::Continue(())
            }

            _ => {
                self.transition(State::Start);
                self.emit(TokenKind::Punctuator)
            }
        }
    }

    // When we enter this state we have consumed a ':' so we can consume a number
    // of other tokens to make valid punctuators
    //
    // Consumes: >?
    //
    // On EOF: Emits a punctuator
    //
    pub(in crate::lexer::implementation) fn handle_punct_colon(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        match next_char {
            '>' => {
                self.consume(1)?;
                self.transition(State::Start);
                self.emit(TokenKind::Punctuator)
            }

            _ => {
                self.transition(State::Start);
                self.emit(TokenKind::Punctuator)
            }
        }
    }

    // When we reach this state we have consumed a '%:' we need to see if we
    // have on alternate pound or two so we need to peek ahead two tokens
    //
    // Consumes: (%:)?
    //
    // On EOF: Issues a Punctuation
    //
    pub(in crate::lexer::implementation) fn handle_alt_pound(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        if next_char == '%' {
            if let Some(CharInfo {
                char: peek_char, ..
            }) = self.try_peek()
            {
                if peek_char == ':' {
                    self.transition(State::Start);
                    self.consume(2)?;
                    return self.emit(TokenKind::Punctuator);
                }
            }
        }
        self.transition(State::Start);
        self.emit(TokenKind::Punctuator)
    }

    // When we reach this state we will have consumed a '-'. We are only going
    // to check if we produce the "->" token othewise we will defer to the
    // PunctDoulbeOrEqual state
    //
    // Consumes: >?
    //
    // On EOF: Emits a Punctuator
    //
    pub(in crate::lexer::implementation) fn handle_punct_minus(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        if next_char == '>' {
            self.consume(1)?;
            self.transition(State::Start);
            self.emit(TokenKind::Punctuator)
        } else {
            self.transition(State::PunctDoubleOrEq('-'));
            ControlFlow::Continue(())
        }
    }

    // When we reach this state we will have consume a '<char>' we can then consume
    // another '<char>' or an '='
    //
    // Consumes: (<char> | =)?
    //
    // On EOF: Emits a Punctuator
    //
    pub(in crate::lexer::implementation) fn handle_punct_double_or_equal(
        &mut self,
        orig: char,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        self.transition(State::Start);
        match next_char {
            '=' => {
                self.consume(1)?;
                self.emit(TokenKind::Punctuator)
            }
            _ if next_char == orig => {
                self.consume(1)?;
                self.emit(TokenKind::Punctuator)
            }
            _ => self.emit(TokenKind::Punctuator),
        }
    }

    // When we reach this state we will have consume a '<char>' we can then consume
    // another '<char>'
    //
    // Consumes: (<char>)?
    //
    // On EOF: Emits a Punctuator
    //
    pub(in crate::lexer::implementation) fn handle_punct_double(
        &mut self,
        orig: char,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        self.transition(State::Start);
        match next_char {
            _ if next_char == orig => {
                self.consume(1)?;
                self.emit(TokenKind::Punctuator)
            }
            _ => self.emit(TokenKind::Punctuator),
        }
    }

    // When we enter this space we will have consumed some non-newline whitespace
    // character.
    //
    // Consumes: <non-newline whitespace>*
    //
    // On EOF: Emit Whitespace
    //
    pub(in crate::lexer::implementation) fn handle_whitespace(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Whitespace)?;

        match next_char {
            '\n' => {
                self.transition(State::Start);
                self.emit(TokenKind::Whitespace)
            }

            _ if next_char.is_ascii_whitespace() => {
                self.consume(1)?;
                ControlFlow::Continue(())
            }

            _ => {
                self.transition(State::Start);
                self.emit(TokenKind::Whitespace)
            }
        }
    }

    // When we enter this state we will have seen a Line ending - which will have
    // been provided by the PeekAdaptor as a `\n`
    //
    // Consumes: <Line Ending: \n, \r, \r\n, \u{b}, \u{85}, \u{2028}, \u{2029}>
    //
    // On EOF: Panic, this is a logic error
    //
    pub(in crate::lexer::implementation) fn handle_eol(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        // This state is now trivial because of the changes to the `PeekAdaptor`
        // which convert all line endings to `\n` for peek and will consume them
        // properly
        self.consume(1);
        self.transition(State::Start);
        self.emit(TokenKind::EOL)
    }

    // When we get here we will have consumed a '/' and it can either be a
    // division or the start of a comment.
    //
    // Consumes (=|/|*)?
    //
    // On EOF: Emits a Punctuator
    //
    pub(in crate::lexer::implementation) fn handle_divide_or_comment(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Punctuator)?;

        match next_char {
            '/' => {
                self.consume(1)?;
                self.transition(State::LineComment);
            }

            '*' => {
                self.consume(1)?;
                self.transition(State::BlockComment);
            }

            '=' => {
                self.consume(1)?;
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }

            _ => {
                self.transition(State::Start);
                return self.emit(TokenKind::Punctuator);
            }
        }

        ControlFlow::Continue(())
    }

    // When we get to this state we will have consumed "//"
    //
    // Consumed: [^\n]*
    //
    // On EOF: Emit a Comment
    //
    pub(in crate::lexer::implementation) fn handle_line_comment(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek_or_emit(TokenKind::Comment)?;

        if next_char == '\n' {
            self.transition(State::Start);
            self.emit(TokenKind::Comment)
        } else {
            self.consume(1);
            ControlFlow::Continue(())
        }
    }

    // When we get to this state we will have consumed '/*'
    //
    // Consumed: .*?*/
    //
    // On EOF: Emit an error
    //
    pub(in crate::lexer::implementation) fn handle_block_comment(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek()?;
        let sec_char = self.peek()?;

        match (next_char, sec_char) {
            ('*', '/') => {
                self.consume(2)?;
                self.transition(State::Start);
                self.emit(TokenKind::Comment)
            }

            (_, _) => {
                self.consume(1)?;
                ControlFlow::Continue(())
            }
        }
    }

    // When we get here we will be partially through some token, we don't know
    // which token only the state it was in. Most errors we don't recover from
    // right now.
    //
    // We will try to recover if we errored during a StringLiteral and will omit
    // whatever we have seen regardless of if the string is properly terminated
    //
    pub(in crate::lexer::implementation) fn handle_error(
        &mut self,
        other_state: State,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        match other_state {
            State::StringLiteral => {
                // let _eol = try_action!(self.peek_or_emit(TokenKind::StringLiteral));
                self.transition(State::Start);

                // TODO: Do a sanity check that the quote isn't immediately
                // after the newline.

                // Insert a fake closing quote were we found the newline
                self.emit_with_modification(TokenKind::StringLiteral, |tok| {
                    let mut tok = tok.to_owned();
                    tok += "\"";

                    tok
                })
            }
            State::QuotedHeaderName | State::BracketedHeaderName => {
                // Check for a potential closing item for the token and consume that
                let _eol = self.peek_or_emit(TokenKind::HeaderName)?;
                let peek_char = self.peek_or_emit(TokenKind::HeaderName)?;

                if (dbg!(peek_char == '>') && dbg!(other_state == State::BracketedHeaderName))
                    || (peek_char == '"' && other_state == State::QuotedHeaderName)
                {
                    self.consume(2)?;
                    self.transition(State::Start);

                    // Queue up a token for the EOL we skipped
                    self.emit_queue.push_back(Ok(Token {
                        kind: TokenKind::EOL,
                        span: Span {
                            bytes: 0..=0,
                            chars: 0..=0,
                            lines: 1..=1,
                        },
                        text: Cow::Owned("\n".into()),
                    }));

                    // Emit the header token but strip out the newline
                    self.emit_with_modification(TokenKind::HeaderName, |tok: Cow<'s, str>| {
                        Cow::Owned(tok.chars().filter(|c| *c != '\n').collect())
                    })
                } else {
                    // We found a `\n` (or other line ending) where we exptected a
                    // `>` or `"`. but didn't find a close immediately after so
                    // emit the token and move on
                    self.transition(State::Start);
                    self.emit(TokenKind::HeaderName)
                }
            }
            _ => {
                // If we are iterating stop here
                self.hit_eof = true;
                ControlFlow::Break(Err(ErrorKind::CannotRecoverFromError))
            }
        }
    }

    fn peek(&mut self) -> ControlFlow<Result<Token<'s>, ErrorKind>, char> {
        self.peek_or_error(ErrorKind::EarlyEOF)
    }

    fn peek_or_error(
        &mut self,
        error: ErrorKind,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>, char> {
        match self.try_peek() {
            Some(value) => ControlFlow::Continue(value.char),
            None => {
                self.transition(State::Error(Box::new(self.state.clone())));
                ControlFlow::Break(Err(error))
            }
        }
    }

    fn peek_or_emit(
        &mut self,
        token_type: TokenKind,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>, char> {
        match self.try_peek() {
            Some(value) => ControlFlow::Continue(value.char),
            None => {
                self.transition(State::Start);
                ControlFlow::Break(Ok(self.build_token(token_type, |x| x)))
            }
        }
    }

    fn try_peek(&mut self) -> Option<CharInfo> {
        self.char_input.peek()
    }

    pub(in crate::lexer::implementation) fn reset_peek(&mut self) {
        self.char_input.reset_peek();
    }

    #[cfg_attr(tarpaulin, skip)]
    fn consume(&mut self, count: usize) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        for _ in 0..count {
            match self.char_input.consume() {
                Ok(()) => {}
                Err(()) => {
                    return ControlFlow::Break(Err(ErrorKind::ConsumedTooManyCharacters));
                }
            };
        }
        ControlFlow::Continue(())
    }

    fn transition(&mut self, state: State) {
        trace!("Transition to: {:?}", state);
        self.state = state;
    }

    fn emit(&mut self, token_type: TokenKind) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        ControlFlow::Break(Ok(self.build_token(token_type, |x| x)))
    }

    fn emit_with_modification<F>(
        &mut self,
        token_type: TokenKind,
        modify: F,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>>
    where
        F: FnOnce(Cow<'s, str>) -> Cow<'s, str>,
    {
        ControlFlow::Break(Ok(self.build_token(token_type, modify)))
    }

    fn build_token<F>(&mut self, token_type: TokenKind, modify: F) -> Token<'s>
    where
        F: FnOnce(Cow<'s, str>) -> Cow<'s, str>,
    {
        match token_type {
            TokenKind::EOF => {
                self.hit_eof = true;
                let (_, end) = self.char_input.get_consumed_range();
                Token {
                    kind: token_type,
                    span: Span {
                        bytes: (self.char_input.get_byte_len())..=(self.char_input.get_byte_len()),
                        chars: (self.char_input.get_num_characters())
                            ..=(self.char_input.get_num_characters()),
                        lines: end.line..=end.line,
                    },
                    text: Cow::Owned("".into()),
                }
            }
            _ => {
                let (start, end) = self.char_input.get_consumed_range();
                Token {
                    kind: token_type,
                    span: Span {
                        bytes: start.byte..=end.byte,
                        chars: start.char_index..=end.char_index,
                        lines: start.line..=end.line,
                    },
                    text: modify(self.char_input.get_consumed()),
                }
            }
        }
    }
}

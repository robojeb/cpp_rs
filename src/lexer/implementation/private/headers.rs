use std::ops::ControlFlow;

use super::State;
use crate::lexer::Lexer;
use crate::lexer::{ErrorKind, Token, TokenKind};
use crate::util::traits::CharSource;

impl<'s, S: CharSource<'s>> Lexer<'s, S> {
    // When we enter this state we will have consumed one '<' in a
    // header name valid situation.
    //
    // Consumes: [^"\n]*>
    //
    // On Eof: Throws an early EOF error
    //
    pub(in crate::lexer::implementation) fn handle_bracketed_header_name(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek()?;

        match next_char {
            '\n' => {
                self.transition(State::Error(Box::new(State::BracketedHeaderName)));
                ControlFlow::Break(Err(ErrorKind::UnexpectedCharacter {
                    found: '\n',
                    expected: vec!['>'],
                }))
            }

            '>' => {
                self.transition(State::Start);
                self.consume(1)?;
                self.emit(TokenKind::HeaderName)
            }

            _ => {
                self.consume(1)?;
                ControlFlow::Continue(())
            }
        }
    }

    // When we enter this state we will have consumed one '"' in a
    // header name valid situation.
    //
    // Consumes: [^"\n]*"
    //
    // On Eof: Throws an early EOF error
    //
    pub(in crate::lexer::implementation) fn handle_quoted_header_name(
        &mut self,
    ) -> ControlFlow<Result<Token<'s>, ErrorKind>> {
        let next_char = self.peek()?;

        match next_char {
            '\n' => {
                self.transition(State::Error(Box::new(State::QuotedHeaderName)));
                ControlFlow::Break(Err(ErrorKind::UnexpectedCharacter {
                    found: '\n',
                    expected: vec!['"'],
                }))
            }

            '"' => {
                self.transition(State::Start);
                self.consume(1)?;
                self.emit(TokenKind::HeaderName)
            }

            _ => {
                self.consume(1)?;
                ControlFlow::Continue(())
            }
        }
    }
}

use super::{private::State, Lexer};
use crate::lexer::{peek_adapter::PeekAdapter, ErrorKind, Token, TokenKind};
use crate::util::traits::CharSource;
use log::trace;
use std::{collections::VecDeque, ops::ControlFlow};

impl<'s, S: CharSource<'s>> Lexer<'s, S> {
    /// Create a new tokenizer based on the input string.
    #[inline]
    pub fn new<I: Into<S>>(input: I) -> Lexer<'s, S> {
        Lexer {
            state: State::Start,

            is_header_valid: false,
            hit_eof: false,

            ext_char_check: None,

            char_input: PeekAdapter::new(input.into()),
            emit_queue: VecDeque::with_capacity(5),
        }
    }

    /// Lex the entire input source in one shot
    ///
    /// This will return a Vector of Tokens (or empty if lexing failed) and
    /// a vector of all the errors encountered.
    ///
    /// # Note
    /// One-shot lexing may not produce correct results with respect to producing
    /// `Kind::HeaderName` tokens. The lexer is not aware of any context and
    /// requires an external driver to determine if `Kind::HeaderName` tokens
    /// are valid to produce.
    ///
    /// This may result in "#include" and "#pragma" directives followed by a
    /// `Kind::StringLiteral` token or in the case of bracketed header names
    /// possibly an error.
    #[inline]
    pub fn lex(mut self) -> (Vec<Token<'s>>, Vec<ErrorKind>) {
        let mut tokens = Vec::new();
        let mut errors = Vec::new();

        loop {
            match self.get_next_token() {
                // EOF indicates the end of the file, push it and then stop
                // processing
                Ok(tok) if tok.kind == TokenKind::EOF => {
                    tokens.push(tok);
                    break;
                }

                // A token was emitted store it
                Ok(tok) => tokens.push(tok),

                // The lexer emitted an error it couldn't recover from so
                // truncate the token output (so the user knows this failed)
                // and then finish lexing
                Err(ErrorKind::CannotRecoverFromError) => {
                    tokens.truncate(0);
                    break;
                }

                // Store an error
                Err(e) => errors.push(e),
            }
        }

        (tokens, errors)
    }

    /// Inform the Lexer that quoted and bracketed header names are valid in the current context
    ///
    /// The Lexer is not designed to know about the pre-processing state.
    /// Because of this it needs to be told if it is allowed to tokenize a
    /// header name. If this is not set all quotes will indicate a quoted string
    /// not a header name.
    ///
    /// This also allows the preprocessor to enable header name tokens in any
    /// location it wants. For example some `#pragma` definitions might want
    /// to allow header name tokens.
    #[inline(always)]
    pub fn set_header_name_allowed(&mut self, is_allowed: bool) {
        self.is_header_valid = is_allowed;
    }

    #[inline(always)]
    pub fn set_identifier_extra_character_check(&mut self, func: fn(char) -> bool) {
        self.ext_char_check = Some(func);
    }

    /// Gets the next token or error from the input source.
    ///
    /// If there is an error in the token source it will return that error.
    /// An error does not indicate that the token stream has terminated.
    /// If another token is requested and it can recover it will return a token.
    /// Otherwise it will return an error indicating it cannot recover.
    ///
    /// ```
    /// use cpp_rs::{util::{Span, char_sources::StrSource}, lexer::{Lexer, Token, TokenKind}};
    /// use std::borrow::Cow;
    ///
    /// let mut lexer : Lexer<StrSource<'static>> = Lexer::new("<");
    ///
    /// # assert_eq!(
    /// #    lexer.get_next_token().unwrap(),
    /// #    Token{
    /// #        kind: TokenKind::Punctuator,
    /// #        span: Span {
    /// #            bytes:0..=0,
    /// #            chars: 0..=0,
    /// #            lines: 1..=1,
    /// #        },
    /// #        text: Cow::Owned("<".into()),
    /// #    }
    /// # );
    /// ```
    ///
    /// Once an EOF token or unrecoverable error token has been generated
    /// EOF or the error will continue to be generated forever.
    pub fn get_next_token(&mut self) -> Result<Token<'s>, ErrorKind> {
        // Some actions (like recovery) may generate a series of tokens and errors
        // so check if that has happened
        if let Some(emit) = self.emit_queue.pop_front() {
            return emit;
        }

        loop {
            trace!("Entering state: {:?}", self.state);
            let temp_state = self.state.clone();

            let action = match temp_state {
                State::Start => self.handle_start(),
                State::QuotedHeaderName => self.handle_quoted_header_name(),
                State::BracketedHeaderName => self.handle_bracketed_header_name(),
                State::IdentOrString => self.handle_ident_or_string(),
                State::IdentOrStringU8 => self.handle_ident_or_string_u8(),
                State::Identifier => self.handle_identifier(),
                State::StringLiteral => self.handle_string_literal(),
                State::Ucn {
                    to_count,
                    return_to,
                } => self.handle_ucn(to_count, return_to),
                State::PPNumber => self.handle_ppnumber(),
                State::PPNumberExponent => self.handle_ppnumber_exponent(),
                State::PPNumberOrDot => self.handle_ppnumber_or_dot(),

                State::PunctGTStart => self.handle_punct_gt(),
                State::PunctLTStart => self.handle_punct_lt(),
                State::PunctAndEqual => self.handle_punct_and_equal(),
                State::PunctPercent => self.handle_punct_percent(),
                State::PunctColon => self.handle_punct_colon(),
                State::PunctMinus => self.handle_punct_minus(),
                State::PunctDoubleOrEq(c) => self.handle_punct_double_or_equal(c),
                State::PunctDouble(c) => self.handle_punct_double(c),
                State::AltPound => self.handle_alt_pound(),
                State::Error(other) => self.handle_error(*other),
                State::Whitespace => self.handle_whitespace(),
                State::EoL => self.handle_eol(),
                State::DivideOrComment => self.handle_divide_or_comment(),
                State::LineComment => self.handle_line_comment(),
                State::BlockComment => self.handle_block_comment(),
            };

            self.reset_peek();
            match action {
                ControlFlow::Continue(_) => {}
                ControlFlow::Break(e) => {
                    trace!("Emitting: {:?}", e);
                    return e;
                }
            }
        }
    }
}

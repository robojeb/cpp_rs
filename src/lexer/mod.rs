// Reference:
// [c11]: http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf

mod error;
mod implementation;
mod peek_adapter;
mod token;

use crate::util::char_sources::{StrSource, StringSource};
pub use error::*;
pub use implementation::*;
pub use token::*;

#[cfg(test)]
mod tests;

/// An alias for a lexer which works over a borrowed `&'s str` reference
///
/// Tokens produced by this lexer will borrow data from the `str` when possible
pub type StrLexer<'s> = Lexer<'s, StrSource<'s>>;
/// An alias for a lexer which owns its source String
///
/// Tokens produced by this lexer also own their own data
pub type StringLexer = Lexer<'static, StringSource>;

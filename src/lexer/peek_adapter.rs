use crate::util::{traits::CharSource, CharInfo, IdxInfo};
use log::trace;
use std::borrow::Cow;
use std::collections::VecDeque;

#[derive(Debug, Default, Clone, Copy)]
pub(super) struct PeekInfo {
    pub byte: usize,
    pub char_index: usize,
    pub line: usize,
}

#[derive(Debug)]
pub(super) struct PeekAdapter<S> {
    source: S,

    peek_index: usize,
    peek_buffer: VecDeque<CharInfo>,

    consumed_start: Option<PeekInfo>,
    consumed_end: PeekInfo,

    current_line: usize,

    consumed: String,
}

impl<'s, S: CharSource<'s>> PeekAdapter<S> {
    pub fn new(source: S) -> PeekAdapter<S> {
        PeekAdapter {
            source,

            peek_index: 0,
            peek_buffer: VecDeque::new(),

            consumed_start: None,
            consumed_end: PeekInfo {
                byte: 0,
                char_index: 0,
                line: 1,
            },

            current_line: 1,

            consumed: String::new(),
        }
    }

    pub fn get_byte_len(&self) -> usize {
        self.source.get_byte_len()
    }

    pub fn get_num_characters(&self) -> usize {
        self.source.get_char_len()
    }

    pub fn peek(&mut self) -> Option<CharInfo> {
        match self.inner_peek() {
            Some(x @ CharInfo { char: '\\', .. }) => match self.inner_peek() {
                // Any single item line ending can be escaped
                Some(CharInfo { char: '\n', .. })
                | Some(CharInfo {
                    char: '\u{000b}', ..
                })
                | Some(CharInfo {
                    char: '\u{000c}', ..
                })
                | Some(CharInfo {
                    char: '\u{0085}', ..
                })
                | Some(CharInfo {
                    char: '\u{2028}', ..
                })
                | Some(CharInfo {
                    char: '\u{2029}', ..
                }) => self.peek(),

                // `\r` can either be just `\r` or `\r\n` so we need to peek more
                Some(CharInfo { char: '\r', .. }) => match self.inner_peek() {
                    Some(CharInfo { char: '\n', .. }) => self.peek(),
                    _ => {
                        self.return_peek();
                        self.peek()
                    }
                },

                Some(_) => {
                    self.return_peek();
                    Some(x)
                }

                // Separate case needed because None doesn't get put in the peek
                // buffer so we have nothing to return
                None => Some(x),
            },

            // Handle `\r` and `\r\n` line endings and return `\n` in all cases
            Some(x @ CharInfo { char: '\r', .. }) => match self.inner_peek() {
                // We found a `\r\n` so return a `\n` in the location of the `\r` but internally
                // progres spast the `\n`
                Some(CharInfo { char: '\n', .. }) => Some(CharInfo { char: '\n', ..x }),
                // Convert stand-alone `\r` to `\n`
                Some(_) => {
                    // Give back the item we peeked because it wasn't a `\r\n`
                    // line ending pair
                    self.return_peek();
                    Some(CharInfo { char: '\n', ..x })
                }
                None => Some(CharInfo { char: '\n', ..x }),
            },

            // Handle Vertical Tab (0xb), Form Feed (0xc), Next Line (0x85),
            // Line separator (0x2028), and Paragraph Separator (0x2029)
            // And convert them all to `\n` for the purposes of peeking
            Some(
                x @ CharInfo {
                    char: '\u{000b}', ..
                },
            )
            | Some(
                x @ CharInfo {
                    char: '\u{000c}', ..
                },
            )
            | Some(
                x @ CharInfo {
                    char: '\u{0085}', ..
                },
            )
            | Some(
                x @ CharInfo {
                    char: '\u{2028}', ..
                },
            )
            | Some(
                x @ CharInfo {
                    char: '\u{2029}', ..
                },
            ) => Some(CharInfo { char: '\n', ..x }),

            out @ Some(_) | out @ None => out,
        }
    }

    pub fn return_peek_n(&mut self, num: usize) {
        assert!(self.peek_index > (num - 1));
        self.peek_index -= num;
    }

    pub fn return_peek(&mut self) {
        // assert!(self.peek_index > 0);
        // self.peek_index -= 1;
        self.return_peek_n(1);
    }

    pub fn reset_peek(&mut self) {
        self.peek_index = 0;
    }

    pub fn consume(&mut self) -> Result<(), ()> {
        match self.inner_consume()? {
            // Check for a line continuation
            CharInfo {
                char: char @ '\\',
                idx:
                    IdxInfo {
                        byte,
                        char_index: index,
                    },
            } => match self.inner_peek() {
                // Check for escaped item
                Some(CharInfo { char: '\n', .. })
                | Some(CharInfo {
                    char: '\u{000b}', ..
                })
                | Some(CharInfo {
                    char: '\u{000c}', ..
                })
                | Some(CharInfo {
                    char: '\u{0085}', ..
                })
                | Some(CharInfo {
                    char: '\u{2028}', ..
                })
                | Some(CharInfo {
                    char: '\u{2029}', ..
                }) => {
                    trace!(
                        "Consume: Skipping escaped newline (byte: {}, char: {})",
                        byte,
                        index
                    );

                    // Increment the line we are on
                    self.current_line += 1;

                    self.inner_consume()?;
                    self.consume()
                }

                // An escaped `\r` could also be a `\r\n` so check here
                Some(CharInfo { char: '\r', .. }) => match self.inner_peek() {
                    Some(CharInfo { char: '\n', .. }) => {
                        self.inner_consume()?;
                        self.inner_consume()?;

                        trace!(
                            "Consume: Skipping escaped newline (byte: {}, char: {})",
                            byte,
                            index
                        );

                        self.current_line += 1;
                        self.consume()
                    }
                    Some(_) | None => {
                        trace!(
                            "Consume: Skipping escaped newline (byte: {}, char: {})",
                            byte,
                            index
                        );

                        self.current_line += 1;

                        self.inner_consume()?;
                        self.consume()
                    }
                },

                // There was a `\` but not a newline so return the peek and
                // just return the `\` as normal
                Some(CharInfo { .. }) | None => {
                    self.reset_peek();
                    self.consumed_start.get_or_insert(PeekInfo {
                        byte,
                        char_index: index,
                        line: self.current_line,
                    });
                    self.consumed_end.byte = byte + char.len_utf8() - 1;
                    self.consumed_end.char_index = index;
                    self.consumed_end.line = self.current_line;

                    self.consumed.push(char);

                    trace!(
                        "Consumed: {:?} ({:?}, {:?})",
                        char,
                        self.consumed_start.unwrap_or_default(),
                        self.consumed_end
                    );
                    Ok(())
                }
            },

            // Consume a `\r\n` as one token because it really should be treated
            // as one, darn Windows crap
            CharInfo {
                char: '\r',
                idx:
                    IdxInfo {
                        byte,
                        char_index: index,
                    },
            } => match self.inner_peek() {
                Some(CharInfo {
                    char: '\n',
                    idx:
                        IdxInfo {
                            byte: byte2,
                            char_index: index2,
                        },
                }) => {
                    self.inner_consume()?;
                    self.consumed_start.get_or_insert(PeekInfo {
                        byte,
                        char_index: index,
                        line: self.current_line,
                    });
                    self.consumed_end.byte = byte2;
                    self.consumed_end.char_index = index2;
                    self.consumed_end.line = self.current_line;

                    self.consumed.push('\r');
                    self.consumed.push('\n');

                    self.current_line += 1;

                    trace!(
                        "Consumed: {:?} ({:?}, {:?})",
                        "\r\n",
                        self.consumed_start.unwrap_or_default(),
                        self.consumed_end
                    );
                    Ok(())
                }
                Some(_) | None => {
                    self.reset_peek();
                    self.consumed_start.get_or_insert(PeekInfo {
                        byte,
                        char_index: index,
                        line: self.current_line,
                    });
                    self.consumed_end.byte = byte;
                    self.consumed_end.char_index = index;
                    self.consumed_end.line = self.current_line;

                    self.consumed.push('\r');

                    self.current_line += 1;

                    trace!(
                        "Consumed: {:?} ({:?}, {:?})",
                        '\r',
                        self.consumed_start.unwrap_or_default(),
                        self.consumed_end
                    );
                    Ok(())
                }
            },

            CharInfo {
                char,
                idx:
                    IdxInfo {
                        byte,
                        char_index: index,
                    },
            } => {
                self.consumed_start.get_or_insert(PeekInfo {
                    byte,
                    char_index: index,
                    line: self.current_line,
                });
                self.consumed_end.byte = byte + char.len_utf8() - 1;
                self.consumed_end.char_index = index;
                self.consumed_end.line = self.current_line;

                self.consumed.push(char);
                trace!(
                    "Consumed: {:?} ({:?}, {:?})",
                    char,
                    self.consumed_start.unwrap_or_default(),
                    self.consumed_end
                );

                // Increment the current line
                if char == '\n'
                    || char == '\u{000b}'
                    || char == '\u{000c}'
                    || char == '\u{0085}'
                    || char == '\u{2028}'
                    || char == '\u{2029}'
                {
                    self.current_line += 1;
                }

                Ok(())
            }
        }
    }

    fn inner_consume(&mut self) -> Result<CharInfo, ()> {
        self.reset_peek();

        self.peek_buffer
            .pop_front()
            .or_else(|| self.source.next())
            .map_or_else(|| Err(()), Ok)
    }

    pub(super) fn get_consumed_range(&self) -> (PeekInfo, PeekInfo) {
        (self.consumed_start.unwrap_or_default(), self.consumed_end)
    }

    pub fn get_consumed(&mut self) -> Cow<'s, str> {
        let consume_len = self.consumed_end.byte - self.consumed_start.unwrap_or_default().byte + 1;

        let out = if self.consumed.len() == consume_len {
            self.source.get_slice(
                IdxInfo {
                    byte: self.consumed_start.unwrap_or_default().byte,
                    char_index: self.consumed_start.unwrap_or_default().char_index,
                },
                IdxInfo {
                    byte: self.consumed_end.byte,
                    char_index: self.consumed_end.char_index,
                },
            )
        } else {
            Cow::Owned(self.consumed.clone())
        };

        self.consumed_start = None;
        self.consumed_end.byte += 1;
        self.consumed_end.char_index += 1;
        self.consumed.truncate(0);

        out
    }

    fn inner_peek(&mut self) -> Option<CharInfo> {
        let out = if self.peek_index < self.peek_buffer.len() {
            Some(self.peek_buffer[self.peek_index].clone())
        } else {
            match self.source.next() {
                Some(x) => {
                    self.peek_buffer.push_back(x);
                    Some(self.peek_buffer[self.peek_index].clone())
                }

                None => return None,
            }
        };

        self.peek_index += 1;
        out
    }
}

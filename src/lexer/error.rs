use crate::util::Span;

/// Indicates the type of error encountered by the Lexer
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ErrorKind {
    /// Encountered EOF before finishing a valid token
    EarlyEOF,
    /// A string literal did not have a valid closing quote before EOL/EOF
    StringLiteralMissingClosingQuote,
    //InvalidIdentifierCharacter(char, usize),
    UnexpectedCharacter {
        found: char,
        expected: Vec<char>,
    },
    ConsumedTooManyCharacters,
    CannotRecoverFromError,
}

/// An error encountered by the Lexer
///
/// All [ErrorKind] values are considered non-fatal for the [Lexer] except
/// [ErrorKind::CannotRecoverFromError].
pub struct Error {
    /// What kind of error occurred
    pub kind: ErrorKind,
    /// What region of the source file was being processed when this error occurred
    pub span: Span,
}

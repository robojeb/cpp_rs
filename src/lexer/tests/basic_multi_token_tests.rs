use crate::{
    lexer::{Lexer, StrLexer, Token, TokenKind},
    util::Span,
};
use std::borrow::Cow;

#[test]
fn dot_ident_rather_than_ppnum() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new(".foo");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned(".".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 1..=3,
                chars: 1..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("foo".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn multi_dot_ppnum() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("..0");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned(".".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 1..=2,
                chars: 1..=2,
                lines: 1..=1,
            },
            text: Cow::Owned(".0".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn greedy_dots() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new(".....");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 0..=2,
                chars: 0..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("...".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned(".".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned(".".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 5..=5,
                chars: 5..=5,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn greedy_dots2() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("...0");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 0..=2,
                chars: 0..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("...".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("0".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

// Make sure that when we find a non-ident digit we can transition properly
#[test]
fn ident_to_num_1() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo.3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=2,
                chars: 0..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("Foo".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 3..=4,
                chars: 3..=4,
                lines: 1..=1,
            },
            text: Cow::Owned(".3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 5..=5,
                chars: 5..=5,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

// In this test the '0' should be part of the identifier not the number
#[test]
fn ident_to_num_2() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Fo0.3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=2,
                chars: 0..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("Fo0".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 3..=4,
                chars: 3..=4,
                lines: 1..=1,
            },
            text: Cow::Owned(".3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 5..=5,
                chars: 5..=5,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3[3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=1,
                chars: 1..=1,
                lines: 1..=1,
            },
            text: Cow::Owned("[".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 2..=2,
                chars: 2..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num2() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3>=3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=2,
                chars: 1..=2,
                lines: 1..=1,
            },
            text: Cow::Owned(">=".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num3() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3>3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=1,
                chars: 1..=1,
                lines: 1..=1,
            },
            text: Cow::Owned(">".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 2..=2,
                chars: 2..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num4() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3<3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=1,
                chars: 1..=1,
                lines: 1..=1,
            },
            text: Cow::Owned("<".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 2..=2,
                chars: 2..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num5() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3<<3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=2,
                chars: 1..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("<<".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num6() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3>>3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=2,
                chars: 1..=2,
                lines: 1..=1,
            },
            text: Cow::Owned(">>".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num7() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3<<=3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=3,
                chars: 1..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("<<=".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 5..=5,
                chars: 5..=5,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num8() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3>>=3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=3,
                chars: 1..=3,
                lines: 1..=1,
            },
            text: Cow::Owned(">>=".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 5..=5,
                chars: 5..=5,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn num_to_punct_to_num9() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("3%3");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 1..=1,
                chars: 1..=1,
                lines: 1..=1,
            },
            text: Cow::Owned("%".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 2..=2,
                chars: 2..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn ternary_operator() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo?3:11e4");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=2,
                chars: 0..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("Foo".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("?".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("3".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 5..=5,
                chars: 5..=5,
                lines: 1..=1,
            },
            text: Cow::Owned(":".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 6..=9,
                chars: 6..=9,
                lines: 1..=1,
            },
            text: Cow::Owned("11e4".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 10..=10,
                chars: 10..=10,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn test_alternate_pound() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("%:%");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 0..=1,
                chars: 0..=1,
                lines: 1..=1,
            },
            text: Cow::Owned("%:".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Punctuator,
            span: Span {
                bytes: 2..=2,
                chars: 2..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("%".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 3..=3,
                chars: 3..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn test_line_comment_after_ident() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo//bar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=2,
                chars: 0..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("Foo".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Comment,
            span: Span {
                bytes: 3..=7,
                chars: 3..=7,
                lines: 1..=1,
            },
            text: Cow::Owned("//bar".into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 8..=8,
                chars: 8..=8,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn test_line_comment_with_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("//bar\nFoo");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Comment,
            span: Span {
                bytes: 0..=4,
                chars: 0..=4,
                lines: 1..=1,
            },
            text: Cow::Borrowed("//bar"),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 5..=5,
                chars: 5..=5,
                lines: 1..=1,
            },
            text: Cow::Borrowed("\n"),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 6..=8,
                chars: 6..=8,
                lines: 2..=2,
            },
            text: Cow::Borrowed("Foo"),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 9..=9,
                chars: 9..=9,
                lines: 2..=2,
            },
            text: Cow::Owned("".into()),
        }
    )
}

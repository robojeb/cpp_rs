use crate::{
    lexer::{ErrorKind, Lexer, StrLexer, Token, TokenKind},
    util::Span,
};
use std::borrow::Cow;

// Run with:
// RUST_LOG=lldt_preproc=trace cargo test

macro_rules! single_token_test {
    ($($name:ident:$input:expr => $tok:expr,$start:expr,$end:expr);*) => {

        $(
            #[test]
            fn $name () {
                let _ = env_logger::try_init();
                let mut lexer : StrLexer<'static> = Lexer::new($input);

                assert_eq!(
                    lexer.get_next_token().unwrap(),
                    Token {
                        kind: $tok,
                        span: Span {
                            bytes: $start..=$end,
                            chars: $start..=$end,
                            lines: 1..=1,
                        },
                        text: Cow::Owned($input.into()),
                    }
                );

                assert_eq!(
                    lexer.get_next_token().unwrap(),
                    Token {
                        kind: TokenKind::EOF,
                        span: Span {
                            bytes: ($end+1)..=($end+1),
                            chars: ($end+1)..=($end+1),
                            lines: 1..=1,
                        },
                        text: Cow::Owned("".into()),
                    }
                )
            }
        )*
    }
}

macro_rules! single_token_header_test {
    ($($name:ident:$input:expr => $tok:expr,$start:expr,$end:expr);*) => {

        $(
            #[test]
            fn $name () {
                let _ = env_logger::try_init();
                let mut lexer : StrLexer<'static> = Lexer::new($input);
                lexer.set_header_name_allowed(true);

                assert_eq!(
                    lexer.get_next_token().unwrap(),
                    Token {
                        kind: $tok,
                        span: Span {
                            bytes: $start..=$end,
                            chars: $start..=$end,
                            lines: 1..=1,
                        },
                        text: Cow::Owned($input.into()),
                    }
                );

                assert_eq!(
                    lexer.get_next_token().unwrap(),
                    Token {
                        kind: TokenKind::EOF,
                        span: Span {
                            bytes: ($end+1)..=($end+1),
                            chars: ($end+1)..=($end+1),
                            lines: 1..=1,
                        },
                        text: Cow::Owned("".into()),
                    }
                )
            }
        )*
    }
}

/*
 * Section: Single punctuator tests
 */

single_token_test!(
    // Punctuation is in the order listed in [c11]: §6.4.6 ¶1
    // to make it easeir to tell if anything is missing
    punct_left_bracket:"[" => TokenKind::Punctuator,0,0;
    punct_right_bracket:"]" => TokenKind::Punctuator,0,0;
    punct_left_paren:"(" => TokenKind::Punctuator,0,0;
    punct_right_paren:")" => TokenKind::Punctuator,0,0;
    punct_left_brace:"{" => TokenKind::Punctuator,0,0;
    punct_right_brace:"}" => TokenKind::Punctuator,0,0;
    punct_dot:"." => TokenKind::Punctuator,0,0;
    punct_push:"->" => TokenKind::Punctuator,0,1;

    punct_increment: "++" => TokenKind::Punctuator,0,1;
    punct_decrement: "--" => TokenKind::Punctuator,0,1;
    punct_bit_and: "&" => TokenKind::Punctuator,0,0;
    punct_mul: "*" => TokenKind::Punctuator,0,0;
    punct_plus: "+" => TokenKind::Punctuator,0,0;
    punct_minux: "-" => TokenKind::Punctuator,0,0;
    punct_bit_not: "~" => TokenKind::Punctuator,0,0;
    punct_bang: "!" => TokenKind::Punctuator,0,0;

    punct_div: "/" => TokenKind::Punctuator,0,0;
    punct_mod: "%" => TokenKind::Punctuator,0,0;
    punct_lshift: "<<" => TokenKind::Punctuator,0,1;
    punct_rshift: ">>" => TokenKind::Punctuator,0,1;
    punct_lt: "<" => TokenKind::Punctuator,0,0;
    punct_gt: ">" => TokenKind::Punctuator,0,0;
    punct_lte: "<=" => TokenKind::Punctuator,0,1;
    punct_gte: ">=" => TokenKind::Punctuator,0,1;
    punct_equal: "==" => TokenKind::Punctuator,0,1;
    punct_bang_eq: "!=" => TokenKind::Punctuator,0,1;
    punct_xor: "^" => TokenKind::Punctuator,0,0;
    punct_bit_or: "|" => TokenKind::Punctuator,0,0;
    punct_logic_and: "&&" => TokenKind::Punctuator,0,1;
    punct_logic_or: "||" => TokenKind::Punctuator,0,1;

    punct_question_mark:"?" => TokenKind::Punctuator,0,0;
    punct_colon: ":" => TokenKind::Punctuator,0,0;
    punct_semi_colon:";" => TokenKind::Punctuator,0,0;
    punct_elipsis:"..." => TokenKind::Punctuator,0,2;

    punct_assign: "=" => TokenKind::Punctuator,0,0;
    punct_mul_eq: "*=" => TokenKind::Punctuator,0,1;
    punct_div_eq: "/=" => TokenKind::Punctuator,0,1;
    punct_mod_eq: "%=" => TokenKind::Punctuator,0,1;
    punct_plus_eq: "+=" => TokenKind::Punctuator,0,1;
    punct_minux_eq: "-=" => TokenKind::Punctuator,0,1;
    punct_lshifteq: "<<=" => TokenKind::Punctuator,0,2;
    punct_rshifteq: ">>=" => TokenKind::Punctuator,0,2;
    punct_bit_and_eq: "&=" => TokenKind::Punctuator,0,1;
    punct_xor_eq: "^=" => TokenKind::Punctuator,0,1;
    punct_bit_or_eq: "|=" => TokenKind::Punctuator,0,1;

    punct_comma:"," => TokenKind::Punctuator,0,0;
    punct_pound: "#" => TokenKind::Punctuator,0,0;
    punct_paste: "##" => TokenKind::Punctuator,0,1;

    punct_alt_left_bracket: "<:" => TokenKind::Punctuator,0,1;
    punct_alt_right_bracket: ":>" => TokenKind::Punctuator,0,1;
    punct_alt_left_brace: "<%" => TokenKind::Punctuator,0,1;
    punct_alt_right_brace: "%>" => TokenKind::Punctuator,0,1;
    punct_alt_pound: "%:" => TokenKind::Punctuator,0,1;
    punct_alt_paste: "%:%:" => TokenKind::Punctuator,0,3;

    ident_simple:"a" => TokenKind::Identifier,0,0;
    ident_underscore:"_" => TokenKind::Identifier,0,0;
    ident_underscore2:"__FLORG" => TokenKind::Identifier,0,6;
    ident_ucn: "a\\u1234" => TokenKind::Identifier,0,6;
    ident_ucn_long: "a\\U12345678" => TokenKind::Identifier,0,10;
    ident_ucn2: "a\\u1234g" => TokenKind::Identifier,0,7;
    ident_ucn_long2: "a\\U12345678g" => TokenKind::Identifier,0,11;
    ident_long:"FooBar_Baz2" => TokenKind::Identifier,0,10;

    ident_that_might_be_string_specifier: "u8" => TokenKind::Identifier,0,1;
    ident_that_might_be_string_specifier2: "U" => TokenKind::Identifier,0,0;
    ident_that_might_be_string_specifier3: "L" => TokenKind::Identifier,0,0;
    ident_that_might_be_string_specifier4: "u88" => TokenKind::Identifier,0,2;
    ident_that_might_be_string_specifier5: "uy" => TokenKind::Identifier,0,1;

    string_literal_simple:"\"Foo\"" => TokenKind::StringLiteral,0,4;
    string_literal_with_simple_specifier: "u\"foo\"" => TokenKind::StringLiteral,0,5;
    string_literal_with_escape: "\"FOO\\\"BAR\"" => TokenKind::StringLiteral,0,9;
    string_literal_with_specifier: "u8\"foo\"" => TokenKind::StringLiteral,0,6;

    ppnumber_zero: "0" => TokenKind::PPNumber,0,0;
    ppnumber_exponent: "0e5" => TokenKind::PPNumber,0,2;
    ppnumber_pos_exponent: "0e+" => TokenKind::PPNumber,0,2;
    ppnumber_neg_exponent: "0e-" => TokenKind::PPNumber,0,2;
    ppnumber_crazy: ".3OO.FOO.3.14159.ebar_" => TokenKind::PPNumber,0,21;
    ppnumber_ucn: "3.\\u1234" => TokenKind::PPNumber,0,7;
    ppnumber_ucn2: "3.\\U12345678" => TokenKind::PPNumber,0,11;

    comment_line_empty: "//" => TokenKind::Comment,0,1;
    comment_line_content: "//foo" => TokenKind::Comment,0,4;
    comment_block_empty: "/**/" => TokenKind::Comment,0,3;
    comment_block_content: "/*foo*/" => TokenKind::Comment,0,6
);

single_token_header_test!(
    header_simple_quoted: r#""foo""# => TokenKind::HeaderName,0,4;
    header_braced_quoted: r#"<foo>"# => TokenKind::HeaderName,0,4
);

// #[test]
// fn test_fail() {
//     let _ = env_logger::try_init();
//     let mut lexer: Strlexer<'static> = lexer::new("a");

//     lexer.get_next_token();

//     assert!(false);
// }
#[test]
fn string_literal_with_no_ending() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new(r#""foo"#);

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::StringLiteralMissingClosingQuote)
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::StringLiteral,
            span: Span {
                bytes: 0..=3,
                chars: 0..=3,
                lines: 1..=1,
            },
            text: Cow::Owned(r#""foo""#.into()),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 4..=4,
                chars: 4..=4,
                lines: 1..=1,
            },
            text: Cow::Owned("".into()),
        }
    )
}

#[test]
fn string_literal_with_no_ending_and_escape() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new(r#""foo\"#);

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::StringLiteralMissingClosingQuote)
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::StringLiteral,
            span: Span {
                bytes: 0..=3,
                chars: 0..=3,
                lines: 1..=1,
            },
            text: Cow::Owned(r#""foo""#.into()),
        }
    );

    //FIXME: This doesn't yet emit a sensible error
}

#[test]
fn ident_that_ends_in_escape() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("foo\\");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=2,
                chars: 0..=2,
                lines: 1..=1,
            },
            text: Cow::Owned("foo".into()),
        }
    );

    //FIXME: This doesn't yet emit a sensible error
}

#[test]
fn string_literal_with_newline_ending() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\"foo\n");

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::StringLiteralMissingClosingQuote)
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::StringLiteral,
            span: Span {
                bytes: 0..=3,
                chars: 0..=3,
                lines: 1..=1,
            },
            text: Cow::Owned("\"foo\"".into()),
        }
    );
}

#[test]
fn bad_escape_in_ident() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("F\\c");

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::UnexpectedCharacter {
            found: 'c',
            expected: vec!['u', 'U'],
        })
    );

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::CannotRecoverFromError),
    );
}

#[test]
fn bad_escape_in_ppnumber() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("0\\c");

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::UnexpectedCharacter {
            found: 'c',
            expected: vec!['u', 'U'],
        })
    );

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::CannotRecoverFromError),
    );
}

#[test]
fn ppnumber_end_in_slash() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("0\\");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1,
            },
            text: Cow::Owned("0".into()),
        }
    );
}

#[test]
fn bad_ucs_character() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("F\\u36y8");

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::UnexpectedCharacter {
            found: 'y',
            expected: vec![
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
                'A', 'B', 'C', 'D', 'E', 'F',
            ],
        })
    );

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::CannotRecoverFromError),
    );
}

#[test]
fn bad_quoted_header() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\"foo\n\"");
    lexer.set_header_name_allowed(true);

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::UnexpectedCharacter {
            found: '\n',
            expected: vec!['"'],
        })
    );

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::HeaderName,
            span: Span {
                bytes: 0..=5,
                chars: 0..=5,
                lines: 1..=2
            },
            text: Cow::Borrowed("\"foo\""),
        })
    );

    // assert_eq!(
    //     lexer.get_next_token(),
    //     Ok(Token {
    //         kind: Kind::EOL,
    //         span: Span {
    //             bytes: 4..=4,
    //             chars: 4..=4,
    //             lines: 1..=1
    //         },
    //         text: Cow::Borrowed("\n"),
    //     })
    // );

    // TODO: The errors cascade because the quote ended up on the next line
    // We should work to make recovery better first
    // assert_eq!(
    //     lexer.get_next_token(),
    //     Ok(Token {
    //         kind: Kind::Punctuator,
    //         span: Span {
    //             bytes: 5..=5,
    //             chars: 5..=5,
    //             lines: 2..=2
    //         },
    //         text: Cow::Borrowed("\""),
    //     })
    // );
}

#[test]
fn bad_bracketed_header() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("<foo\n>");
    lexer.set_header_name_allowed(true);

    assert_eq!(
        lexer.get_next_token(),
        Err(ErrorKind::UnexpectedCharacter {
            found: '\n',
            expected: vec!['>'],
        })
    );

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::HeaderName,
            span: Span {
                bytes: 0..=5,
                chars: 0..=5,
                lines: 1..=2
            },
            text: Cow::Borrowed("<foo>"),
        })
    );

    // FIXME: Should probably emit an EOL as part of the recovery
    // assert_eq!(
    //     lexer.get_next_token(),
    //     Ok(Token {
    //         kind: Kind::EOL,
    //         span: Span {
    //             bytes: 4..=4,
    //             chars: 4..=4,
    //             lines: 1..=1
    //         },
    //         text: Cow::Borrowed("\n"),
    //     })
    // );

    // assert_eq!(
    //     lexer.get_next_token(),
    //     Ok(Token {
    //         kind: Kind::Punctuator,
    //         span: Span {
    //             bytes: 5..=5,
    //             chars: 5..=5,
    //             lines: 2..=2
    //         },
    //         text: Cow::Borrowed(">"),
    //     })
    // );
}

#[test]
fn windows_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\r\n");

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 0..=1,
                chars: 0..=1,
                lines: 1..=1
            },
            text: Cow::Borrowed("\r\n"),
        })
    )
}

#[test]
fn osx_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\r");

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1
            },
            text: Cow::Borrowed("\r"),
        })
    )
}

#[test]
fn vertical_tab_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\u{b}");

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1
            },
            text: Cow::Borrowed("\u{b}"),
        })
    )
}

#[test]
fn form_feed_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\u{c}");

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 0..=0,
                chars: 0..=0,
                lines: 1..=1
            },
            text: Cow::Borrowed("\u{c}"),
        })
    )
}

#[test]
fn next_line_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\u{85}");

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 0..=1,
                chars: 0..=0,
                lines: 1..=1
            },
            text: Cow::Borrowed("\u{85}"),
        })
    )
}

#[test]
fn line_sep_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\u{2028}");

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 0..=2,
                chars: 0..=0,
                lines: 1..=1
            },
            text: Cow::Borrowed("\u{2028}"),
        })
    )
}

#[test]
fn paragraph_sep_eol() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\u{2029}");

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::EOL,
            span: Span {
                bytes: 0..=2,
                chars: 0..=0,
                lines: 1..=1
            },
            text: Cow::Borrowed("\u{2029}"),
        })
    )
}

#[test]
fn extended_ident() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("a→b");

    lexer.set_identifier_extra_character_check(|c| c == '→');

    assert_eq!(
        lexer.get_next_token(),
        Ok(Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=4,
                chars: 0..=2,
                lines: 1..=1
            },
            text: Cow::Borrowed("a→b"),
        })
    )
}

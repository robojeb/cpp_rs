use crate::{
    lexer::{Lexer, StrLexer, Token, TokenKind},
    util::Span,
};
use std::borrow::Cow;

#[test]
fn escape_newline_eof() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\\\n");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 2..=2,
                chars: 2..=2,
                lines: 1..=1,
            },
            text: Cow::Borrowed(""),
        }
    );
}

#[test]
fn escape_identifier() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo\\\nBar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=7,
                chars: 0..=7,
                lines: 1..=2,
            },
            text: Cow::Borrowed("FooBar"),
        }
    );
}

#[test]
fn string_literal_with_escaped_newline() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("\"foo\\\nbar\"");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::StringLiteral,
            span: Span {
                bytes: 0..=9,
                chars: 0..=9,
                lines: 1..=2,
            },
            text: Cow::Borrowed("\"foobar\""),
        }
    );
}

#[test]
fn ident_to_num_with_escaped_newline() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foobar\\\n.0foo");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=5,
                chars: 0..=5,
                lines: 1..=1,
            },
            text: Cow::Borrowed("Foobar"),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::PPNumber,
            span: Span {
                bytes: 8..=12,
                chars: 8..=12,
                lines: 2..=2,
            },
            text: Cow::Borrowed(".0foo"),
        }
    );
}

#[test]
fn comment_with_escaped_line() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("//foo\\\nbar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Comment,
            span: Span {
                bytes: 0..=9,
                chars: 0..=9,
                lines: 1..=2,
            },
            text: Cow::Borrowed("//foobar"),
        }
    );

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::EOF,
            span: Span {
                bytes: 10..=10,
                chars: 10..=10,
                lines: 2..=2,
            },
            text: Cow::Borrowed(""),
        }
    );
}

#[test]
fn escape_identifier_windows() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo\\\r\nBar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=8,
                chars: 0..=8,
                lines: 1..=2,
            },
            text: Cow::Borrowed("FooBar"),
        }
    );
}

#[test]
fn escape_identifier_osx() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo\\\rBar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=7,
                chars: 0..=7,
                lines: 1..=2,
            },
            text: Cow::Borrowed("FooBar"),
        }
    );
}

#[test]
fn escape_identifier_vt() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo\\\u{b}Bar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=7,
                chars: 0..=7,
                lines: 1..=2,
            },
            text: Cow::Borrowed("FooBar"),
        }
    );
}

#[test]
fn escape_identifier_nextline() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo\\\u{85}Bar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=8,
                chars: 0..=7,
                lines: 1..=2,
            },
            text: Cow::Borrowed("FooBar"),
        }
    );
}

#[test]
fn escape_identifier_linesep() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo\\\u{2028}Bar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=9,
                chars: 0..=7,
                lines: 1..=2,
            },
            text: Cow::Borrowed("FooBar"),
        }
    );
}

#[test]
fn escape_identifier_parsep() {
    let _ = env_logger::try_init();
    let mut lexer: StrLexer<'static> = Lexer::new("Foo\\\u{2029}Bar");

    assert_eq!(
        lexer.get_next_token().unwrap(),
        Token {
            kind: TokenKind::Identifier,
            span: Span {
                bytes: 0..=9,
                chars: 0..=7,
                lines: 1..=2,
            },
            text: Cow::Borrowed("FooBar"),
        }
    );
}
